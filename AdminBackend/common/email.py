import requests


class Send_Email(object):

    def email(email,password,token):
        url = "http://192.168.6.115:5000/api/v1/mail"
        if token != 0:
            data = {
                "to": email,
                "from": "bit@bitinka.com",
                "client": "Bitinka",
                "subject": "Asignar contrasena para completar su registro",
                "html": token,
                "text": "text",
                "fromname": "Bitinka"
            }
        else:
            data = {
                "to": email,
                "from": "bit@bitinka.com",
                "client": "Bitinka",
                "subject": "Password Generico",
                "html": password.upper(),
                "text": "text",
                "fromname": "Bitinka"
            }
        return requests.post(url, data=data)
