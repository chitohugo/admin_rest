from rest_framework.response import Response
from rest_framework import status
from itsdangerous import URLSafeSerializer, URLSafeTimedSerializer, SignatureExpired
from AdminBackend.common.email import Send_Email
from AdminBackend.settings import SECRET_KEY
from users.models import Admin


class Token_Itsdangerous(object):
    def generated_token(id):
        secret_key = SECRET_KEY
        serializer = URLSafeTimedSerializer(secret_key)
        token = serializer.dumps(id)
        return token

    def confirm_token(token, expiration=300):
        secret_key = SECRET_KEY
        serializer = URLSafeTimedSerializer(secret_key)
        try:
            id = serializer.loads(token, max_age=expiration)
            data = dict(status=True, token=id)
        except:
            id = serializer.loads(token)
            data = dict(status=False, token=id)
            return data
        return data
