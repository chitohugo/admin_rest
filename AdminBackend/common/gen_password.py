from random import SystemRandom


class Gen_Password(object):

    def password():
        length = 18
        values = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#%&+"
        pass_encrypted = SystemRandom()
        password_gen = ''
        while length != 0:
            password_gen = password_gen + pass_encrypted.choice(values)
            length = length - 1
        return password_gen