from django.http import Http404
from users.models import Admin
from groups.models import Groups
from profiles.models import Profiles, ProfileModules
from modules.models import Modules


class Users(object):
    def get_object(id):
        try:
            return Admin.objects.get(id_admin=id, is_active__gte=1)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_active(id):
        try:
            return Admin.objects.get(id_admin=id, is_active=1)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_username(username):
        try:
            return Admin.objects.get(username=username, is_active=1)
        except Admin.DoesNotExist:
            raise Http404

    def get_object_email(email):
        try:
            return Admin.objects.get(email=email, is_active=1)
        except Admin.DoesNotExist:
            raise Http404


class Group(object):
    def get_object(id):
        try:
            return Groups.objects.get(id_group=id, status_id__gte=1)
        except Groups.DoesNotExist:
            raise Http404

    def get_object_active(id):
        try:
            return Groups.objects.get(id_group=id, status_id=1)
        except Groups.DoesNotExist:
            raise Http404


class Profile(object):
    def get_object(id):
        try:
            return Profiles.objects.get(id_profile=id, status_id__gte=1)
        except Profiles.DoesNotExist:
            raise Http404

    def get_object_active(id):
        try:
            return Profiles.objects.get(id_profile=id, status_id=1)
        except Profiles.DoesNotExist:
            raise Http404


class Module(object):
    def get_object(id):
        try:
            return Modules.objects.get(id_module=id)
        except Modules.DoesNotExist:
            raise Http404


class ProfileModule(object):
    def get_object(profile, module):
        try:
            return ProfileModules.objects.get(profile=profile, module=module)
        except ProfileModules.DoesNotExist:
            return None
