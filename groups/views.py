from django.utils import timezone
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from users.models import Admin
from profiles.models import Profiles
from groups.models import Groups, GroupAdmin, GroupProfile
from groups.serializers import GroupSerializer, \
    UsersMinSerializer, GroupAdminSerializer, ProfilesMinSerializer, GroupProfileSerializer
from AdminBackend.common import Users, Group
from security.permissions import IsAuthenticatedActive, HasAccessModuleFunc


class GroupSolo(viewsets.ModelViewSet):
    """
        @api {get} /groups/:id Obtener un Grupo por su id.
        @apiHeader {String} Token User unique access-key.
        @apiName RetrieveGroupId
        @apiGroup Group
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo en la url del request.
        @apiSuccess (200) {String} response OK
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok
            {
                "id_group": 1,
                "name_group": "Admin",
                "description": "Administrador",
                "status_id": 1
            }           
        @apiError (404) {Json} errors Indicando que el Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found            
            {
                "detail": "No encontrado."
            }
    """
    """
        @api {get} /groups/:id/detail Obtener detalles de un Grupo por su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName DetailGroupId
        @apiGroup Group
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc 
        @apiParam {Number} id - id del Grupo en la url del request.
        @apiSuccess (200) {String} response OK
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok
            {
                "id_group": 1,
                "name_group": "Admin",
                "description": "Administrador",
                "status_id": 1
                "admin_create": "admin",
                "admin_update": "admin"
            }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                "detail": "No encontrado."
            }
    """
    """
        @api {put} /groups/:id Actualizar el grupo por su id.
        @apiParamExample {Json} Request-Group:
            {
                "name_group": "Admin",
                "description": "Administración",
                "status_id": 1
            }
        @apiHeader {String} Token Users unique access-key.
        @apiName UpdateGroupId
        @apiGroup Group
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo en la url del request.
        @apiParam {String} name_group - Nombre del grupo, si se cambia debe ser único, sino se debe incluir igualmente, ya que es requerido.
        @apiParam {String} description - Descripción del grupo. Si se incluye debe contener 1 o más caracteres. No es requerido.
        @apiParam {Number} status_id - Estatus de grupo. 1: Activo; 2: Inactivo. No es requerido.
        @apiSuccess (202) {String} response Accepted
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 202 Accepted
            {                
                "id_group": 1,
                "name_group": "Admin",
                "description": "Administración",
                "status_id": 1                
            }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos, etc.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }       
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "name_group": [
                        "groups con esta name group ya existe."
                ]
            }			
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "name_group": [
                        "Este campo es requerido."
                ]
            }			 
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "description": [
                        "Este campo no puede estar en blanco."
                ]
            }       
    """
    """
        @api {put} /groups/:id/status Actualizar el estatus de un grupo por su id.       
        @apiHeader {String} Token Users unique access-key.
        @apiName StatusGroupId
        @apiGroup Group
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParamExample {Json} Request-Status:
            {
                "status_id": 2
            }
        @apiParam {Number} id - id del Grupo en la url del request.
        @apiParam {Number} status_id - Estatus del grupo. 1: Activo; 2: Inactivo. Es requerido.       
        @apiSuccess (202) {String} response Accepted
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos son requeridos o tienen valores no válidos etc.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "status_id": [
                    "Este campo es requerido."
                ]
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "status_id": [
                    "Introduzca un número entero válido."
                ]
            }       
    """
    """
        @api {delete} /groups/:id Elimina un grupo por su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName DeleteGroupId
        @apiGroup Group
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo en la url del request.
        @apiSuccess (204) {String} response No Content
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 204 No Content
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
    """

    module = 'groups'
    map_action = {'retrieve': 'pro_consul', 'detail': 'pro_consul', 'create': 'pro_add', 'update': 'pro_edit',
                  'status': 'pro_edit', 'destroy': 'pro_erase'}
    queryset = None
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def retrieve(self, request, id):
        """Permite obtener un grupo dado su id"""
        instance = Group.get_object(id)
        queryset = Groups.objects.get(id_group=instance.id_group)
        serializer = self.get_serializer(queryset, fields=('id_group', 'name_group', 'description', 'status_id'),
                                         many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def detail(self, request, id):
        """Permite obtener detalles del grupo dado su id"""
        instance = Group.get_object(id)
        queryset = Groups.objects.get(id_group=instance.id_group)
        serializer = self.get_serializer(queryset, fields=('id_group', 'name_group', 'description', 'status_id',
                                                           'admin_create', 'admin_update'), many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, id):
        """Permite actualizar un grupo dado su id"""
        instance = Group.get_object(id)
        serializer = self.get_serializer(instance, fields=('id_group', 'name_group', 'description', 'status_id'),
                                         data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def perform_update(self, serializer, request):
        serializer.save(
            admin_id_update=request.user,
            modified_date=timezone.now()
        )

    def status(self, request, id):
        """Permite solo actualizar el status del grupo a activo/inactivo"""
        try:
            status_id = request.data["status_id"]
        except Exception as e:
            return Response({"status_id": ["Este campo es requerido."]}, status=status.HTTP_400_BAD_REQUEST)
        instance = Group.get_object(id)
        serializer = self.get_serializer(instance, fields=('status_id', ), data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(status=status.HTTP_202_ACCEPTED)

    def destroy(self, request, id):
        """Permite borrar de manera logica al grupo dado su id"""
        instance = Group.get_object(id)
        Groups.objects.filter(id_group=instance.id_group).update(
            status_id=0,
            admin_id_update=request.user,
            modified_date=timezone.now()
        )
        return Response(status=status.HTTP_202_ACCEPTED)


'''Clase de la api para listar los grupos y crear un grupo'''


class GroupList(viewsets.ModelViewSet):

    """
        @api {get} /groups Listar Grupos Registrados
        @apiHeader {String} Token User unique access-key.
        @apiName ListGroups
        @apiGroup GroupList
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiSuccess (200) {String} response Listado de Grupos registrados.
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok        
            [
                {
                    "id_group": 2,
                    "name_group": "Admin",
                    "description": "admin",
                    "status_id": 1,
                    "admin_create": "admin1"
                },
                {
                    "id_group": 1,
                    "name_group": "Admin2",
                    "description": "admin",
                    "status_id": 1,
                    "admin_create": "admin1"
                }
            ]
    """
    """ 
        @api {post} /groups Registrar Grupo
        @apiParamExample {Json} Request-Create:            
            {
                "name_group": "Admin",
                "description": "admin"
            }
        @apiHeader {String} Token User unique access-key.
        @apiName PostGroup
        @apiGroup Groups
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {String} name_group - Nombre del grupo, debe ser único. Es requerido.
        @apiParam {String} description - Descripcion del Grupo. Si se incluye debe tener 1 o más caracteres o tener valor null. No es requerido.        
        @apiSuccess (201) {String} response Created.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 201 Created
            {
                "id": 1
            }    
        @apiError (400) {Json} errors Indicando que campos ya existen y/o estan en blanco
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                    "name_group": [
                        "groups con esta name group ya existe."
                    ],
                    "description": [
                        "Este campo no puede estar en blanco."
                    ]
                }
            }
            HTTP/1.1 400 Bad Request
            {
               {
                 "name_group": [
                        "Este campo no puede estar en blanco."
                    ]
               }
            }    
            HTTP/1.1 400 Bad Request
                {
                   {
                        "description": [
                            "Este campo no puede estar en blanco."
                        ]
                   }
                }          
    """

    module = 'groups'
    map_action = {'list': 'pro_consul', 'create': 'pro_add'}
    queryset = Groups.objects.filter(status_id__gte=1).order_by('-modified_date')
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def list(self, request, *args, **kwargs):
        """Permite obtener todos los grupos"""
        queryset = self.filter_queryset(self.get_queryset())
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #      serializer = self.get_serializer(page, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'date_joined'), many=True)
        #      return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, fields=('id_group', 'name_group', 'description', 'status_id',
                                                           'admin_create'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Permite crear un grupo"""
        serializer = self.get_serializer(data=request.data, fields=('id_group', 'name_group', 'description'))
        serializer.is_valid(raise_exception=True)
        group = self.perform_create(serializer, request)
        return Response({'id': group.id_group}, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer, request):
        return serializer.save(
            status_id=1,
            admin_id_create=request.user,
            admin_id_update=request.user)


class GroupUsersList(viewsets.ModelViewSet):
    """
        @api {get} /groups/:id/users/in Listar Usuarios pertenencientes a un Grupo.
        @apiHeader {String} Token User unique access-key.
        @apiName GetGroupsUsersIn
        @apiGroup GroupsUsers
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (200) {Json} response Listado de Usuarios pertenencientes a un Grupo.
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok           
              [
                {
                    "id_admin": 1,
                    "username": "admin",
                    "is_active": 1
                }
              ]
           
           HTTP/1.1 200 Ok
           {
              []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
        @api {get} /groups/:id/users/out Listar Usuario No pertenencientes a un Grupo.
        @apiHeader {String} Token User unique access-key.
        @apiName GetGroupsUsersOut
        @apiGroup GroupsUsers
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (200) {Json} response Listado de Usuario no pertenencientes a un Grupo.
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_admin": 1,
                    "username": "admin",
                    "is_active": 1
                }
              ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
        @api {post} /groups/:id/users Crear Grupo a Usuarios
        @apiParamExample {Json} Request-Users-Group:
           {
                {"users": [1,2,...]}
           }
        @apiHeader {String} Token User unique access-key.
        @apiName PostGroupsUsers
        @apiGroup GroupsUsers
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (201) {Json} response
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 201 Created
           {
                []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
        @apiError (400) {Json} errors Indicando que clave primaria es invalida.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {
                {
                    "admin": [
                        "Clave primaria \":id\" inválida - objeto no existe."
                    ]
                }
           }
        @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """

    """
        @api {delete} /groups/:id/users Eliminar Usuarios pertenencientes a Grupos.
        @apiParamExample {Json} Request-Group-Users:
           {
                "users": [1,2]
           }
        @apiHeader {String} Token User unique access-key.
        @apiName DeleteGroupsUsers
        @apiGroup GroupsUsers
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (202) {String} response Accepted
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
        @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """
    module = 'groups'
    map_action = {'list_users_in': 'pro_consul', 'list_users_out': 'pro_consul', 'create': 'pro_add',
                  'destroy': 'pro_erase'}
    queryset = None
    serializer_class = UsersMinSerializer
    permission_classes = (IsAuthenticatedActive, )

    def list_users_in(self, request, id):
        instance = Group.get_object(id)
        self.queryset = Admin.objects.filter(id_admin__in=GroupAdmin.objects.filter(group_id=instance.id_group).
                                             values('admin_id')).filter(is_active__gte=1).order_by('username')
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(serializer.data)

    def list_users_out(self, request, id):
        instance = Group.get_object(id)
        self.queryset = Admin.objects.exclude(id_admin__in=GroupAdmin.objects.filter(group_id=instance.id_group).
                                              values('admin_id')).filter(is_active__gte=1).order_by('username')
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(serializer.data)

    def create(self, request, id, *args, **kwargs):
        response = dict()
        response['errors'] = []
        instance = Group.get_object_active(id)
        try:
            users = request.data["users"]
            for user in users:
                groupuser = None
                groupuser = GroupAdmin.objects.filter(group=id, admin=user)
                if not groupuser:
                    serializer = GroupAdminSerializer(data={'group': instance.id_group, 'admin': user})
                    try:
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            response['errors'].append(serializer.errors)
                    except Exception as e:
                        response['errors'].append(serializer.errors)
            status_response = status.HTTP_201_CREATED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, id):
        response = dict()
        response['errors'] = []
        instance = Group.get_object_active(id)
        try:
            users = request.data["users"]
            for user in users:
                try:
                    GroupAdmin.objects.filter(group=instance.id_group, admin=user).delete()
                except Exception as e:
                    response['errors'].append({"error": ["No se pudo realizar la operacion."]})
            status_response = status.HTTP_202_ACCEPTED if not len(
                response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)


class GroupProfilesList(viewsets.ModelViewSet):
    """
        @api {get} /groups/:id/profiles/in Listar Perfiles pertenencientes a un Grupo.
        @apiHeader {String} Token User unique access-key.
        @apiName GetGroupsProfilesIn
        @apiGroup GroupsProfiles
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (200) {Json} response Listado de Perfiles pertenencientes a un Grupo.
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_profile": 1,
                    "name_profile": "Administraccion",
                    "status_id": 1
                }
              ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
        @api {get} /groups/:id/profiles/out Listar Perfiles No pertenencientes a un Grupo.
        @apiHeader {String} Token User unique access-key.
        @apiName GetGroupsProfilesOut
        @apiGroup GroupsProfiles
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (200) {Json} response Listado de Perfiles no pertenencientes a un Grupo.
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_profile": 1,
                    "name_profile": "Administraccion",
                    "status_id": 1
                }
              ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
        @api {post} /groups/:id/profiles Crear Grupo a Perfiles
        @apiParamExample {Json} Request-User-Group:
           {
                {"profiles": [1,2,...]}
           }
        @apiHeader {String} Token User unique access-key.
        @apiName PostGroupsProfiles
        @apiGroup GroupsProfiles
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (201) {Json} response
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 201 Created
           {
                []
           }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
        @apiError (400) {Json} errors Indicando que clave primaria es invalida.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {
                {
                    "profile": [
                        "Clave primaria \":id\" inválida - objeto no existe."
                    ]
                }
           }
        @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """

    """
        @api {delete} /groups/:id/profiles Eliminar Grupos pertenencientes a Usuario.
        @apiParamExample {Json} Request-User-Group:
           {
                "profiles": [1,2,...]
           }
        @apiHeader {String} Token User unique access-key.
        @apiName DeleteGroupsProfiles
        @apiGroup GroupsProfiles
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Grupo.
        @apiSuccess (204) {String} response No Content
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 204 No Content
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
        @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
        @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """
    queryset = None
    serializer_class = ProfilesMinSerializer
    permission_classes = (IsAuthenticatedActive,)

    def list_profiles_in(self, request, id):
        """Permite obtener los perfiles asociados a un grupo dado su id"""
        instance = Group.get_object(id)
        self.queryset = Profiles.objects.filter(id_profile__in=GroupProfile.objects.filter(group_id=instance.id_group).
                                                values('profile_id')).filter(status_id__gte=1).order_by('name_profile')
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(serializer.data)

    def list_profiles_out(self, request, id):
        """Permite obtener los perfiles no asociados a un grupo dado su id"""
        instance = Group.get_object(id)
        self.queryset = Profiles.objects.exclude(id_profile__in=GroupProfile.objects.filter(group_id=instance.id_group).
                                                 values('profile_id')).filter(status_id__gte=1).order_by('name_profile')
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(serializer.data)

    def create(self, request, id, *args, **kwargs):
        """Permite agregar un lote de perfiles a un grupo dado su id"""
        response = dict()
        response['errors'] = []
        instance = Group.get_object_active(id)
        try:
            profiles = request.data["profiles"]
            for profile in profiles:
                grouprofile = None
                grouprofile = GroupProfile.objects.filter(group=instance.id_group, profile=profile)
                if not grouprofile:
                    serializer = GroupProfileSerializer(data={'group': instance.id_group, 'profile': profile})
                    try:
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            response['errors'].append(serializer.errors)
                    except Exception as e:
                        response['errors'].append(serializer.errors)
            status_response = status.HTTP_201_CREATED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    '''Este metodo solo se usa para eliminar el recurso, en este caso borrar la relacion de 
            un grupo con un lote de perfiles'''
    def destroy(self, request, id):
        """Permite borrar un lote de perfiles de un grupo dado su id"""
        response = dict()
        response['errors'] = []
        instance = Group.get_object_active(id)
        try:
            profiles = request.data["profiles"]
            for profile in profiles:
                try:
                    GroupProfile.objects.filter(group=instance.id_group, profile=profile).delete()
                except Exception as e:
                    response['errors'].append({"error": ["No se pudo realizar la operacion."]})
            status_response = status.HTTP_204_NO_CONTENT if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

