
from rest_framework import serializers

from groups.models import Groups, GroupAdmin, GroupProfile
from users.models import Admin
from profiles.models import Profiles


# Create your models here.

class DynamicSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class GroupSerializer(DynamicSerializer):
    admin_create = serializers.ReadOnlyField(source='admin_id_create.username')
    admin_update = serializers.ReadOnlyField(source='admin_id_update.username')

    class Meta:
        model = Groups
        fields = ('id_group', 'name_group', 'description', 'status_id', 'create_date', 'admin_id_create',
                  'modified_date', 'admin_id_update', 'admin_create', 'admin_update')

    def validate_status_id(self, status_id):
        if status_id not in (1,2):
            raise serializers.ValidationError('Asegúrese de que este valor sea 1 o 2.')
        return status_id

# Serializers GroupsAdmin

class UsersMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        fields = ('id_admin', 'username', 'is_active')


class GroupAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupAdmin
        fields = ('group', 'admin')


# Serializers GroupsProfiles

class ProfilesMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profiles
        fields = ['id_profile', 'name_profile', 'status_id']


class GroupProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupProfile
        fields = ('group', 'profile')

