# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone

from users.models import Admin
from profiles.models import Profiles


class Groups(models.Model):
    id_group = models.AutoField(primary_key=True)
    name_group = models.CharField(max_length=45, unique=True)
    description = models.CharField(max_length=120, null=True)
    status_id = models.IntegerField(default=1, validators=[MaxValueValidator(2), MinValueValidator(0)])
    admin_id_create = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='admincreategroups')
    create_date = models.DateTimeField(default=timezone.now)
    admin_id_update = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='adminupdategroups')
    modified_date = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'groups'

    def __str__(self):
        return self.id_group


class GroupAdmin(models.Model):
    id_group_admin = models.AutoField(primary_key=True)
    group = models.ForeignKey(Groups, on_delete=models.CASCADE, related_name='group_admin_group')
    admin = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='group_admin_admin')
    admin_id_create = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'group_admin'
        unique_together = ('group', 'admin')

    def __str__(self):
        return self.id_group_admin


class GroupProfile(models.Model):
    id_group_profile = models.AutoField(primary_key=True)
    group = models.ForeignKey(Groups, on_delete=models.CASCADE, related_name='group_profile_group')
    profile = models.ForeignKey(Profiles, on_delete=models.CASCADE, related_name='group_profile_profile')

    class Meta:
        db_table = 'group_profile'
        unique_together = ('group', 'profile')

    def __str__(self):
        return self.id_group_profile
