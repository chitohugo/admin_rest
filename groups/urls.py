from django.conf.urls import url
from groups import views

group_list = views.GroupList.as_view({
    'get': 'list',
    'post': 'create'
})

group_solo = views.GroupSolo.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

group_detail = views.GroupSolo.as_view({
    'get': 'detail'
})

group_status = views.GroupSolo.as_view({
    'put': 'status'
})

group_users = views.GroupUsersList.as_view({
    'post': 'create',
    'delete': 'destroy'
})

group_users_in = views.GroupUsersList.as_view({
    'get': 'list_users_in'
})

group_users_out = views.GroupUsersList.as_view({
    'get': 'list_users_out'
})

group_profiles = views.GroupProfilesList.as_view({
    'post': 'create',
    'delete': 'destroy'
})

group_profiles_in = views.GroupProfilesList.as_view({
    'get': 'list_profiles_in'
})

group_profiles_out = views.GroupProfilesList.as_view({
    'get': 'list_profiles_out'
})

urlpatterns = [
    url(r'^groups$', group_list, name='groups-list'),
    url(r'^groups/(?P<id>[0-9]+)$', group_solo, name='group-solo'),
    url(r'^groups/(?P<id>[0-9]+)/detail$', group_detail, name='group-detail'),
    url(r'^groups/(?P<id>[0-9]+)/status$', group_status, name='group-status'),

    #GroupsUsers
    url(r'^groups/(?P<id>[0-9]+)/users$', group_users, name='group-users'),
    url(r'^groups/(?P<id>[0-9]+)/users/in$', group_users_in, name='group-users-in'),
    url(r'^groups/(?P<id>[0-9]+)/users/out$', group_users_out, name='group-users-out'),

    #GroupsProfiles
    url(r'^groups/(?P<id>[0-9]+)/profiles$', group_profiles, name='group-profiles'),
    url(r'^groups/(?P<id>[0-9]+)/profiles/in$', group_profiles_in, name='group-profiles-in'),
    url(r'^groups/(?P<id>[0-9]+)/profiles/out$', group_profiles_out, name='group-profiles-out'),
]
