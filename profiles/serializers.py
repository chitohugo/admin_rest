from rest_framework import serializers

from modules.models import Modules
from profiles.models import Profiles, ProfileModules


class DynamicSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class ProfileSerializer(DynamicSerializer):
    admin_create = serializers.ReadOnlyField(source='admin_id_create.username')
    admin_update = serializers.ReadOnlyField(source='admin_id_update.username')

    class Meta:
        model = Profiles
        fields = ('id_profile', 'name_profile', 'description', 'status_id', 'create_date', 'modified_date',
                  'admin_id_create', 'admin_id_update', 'admin_create', 'admin_update')

    def validate_status_id(self, status_id):
        if status_id not in (1,2):
            raise serializers.ValidationError('Asegúrese de que este valor sea 1 o 2.')
        return status_id


class ProfileModuleSerializer(DynamicSerializer):
    module_name = serializers.ReadOnlyField(source='module.name_module')
    mod_consult = serializers.ReadOnlyField(source='module.mod_consult')
    mod_add = serializers.ReadOnlyField(source='module.mod_add')
    mod_edit = serializers.ReadOnlyField(source='module.mod_edit')
    mod_export = serializers.ReadOnlyField(source='module.mod_export')
    mod_erase = serializers.ReadOnlyField(source='module.mod_erase')
    mod_import = serializers.ReadOnlyField(source='module.mod_import')
    mod_download = serializers.ReadOnlyField(source='module.mod_download')

    class Meta:
        model = ProfileModules
        fields = ('name_profile_modules', 'module', 'module_name', 'mod_consult', 'pro_consul', 'mod_add', 'pro_add',
                  'mod_edit', 'pro_edit', 'mod_export', 'pro_export', 'mod_erase', 'pro_erase', 'mod_import',
                  'pro_import', 'mod_download', 'pro_download')


class ModuleSerializer(DynamicSerializer):
    name_profile_modules = serializers.CharField(default="")
    module = serializers.ReadOnlyField(source='id_module')
    module_name = serializers.ReadOnlyField(source='name_module')
    pro_consul = serializers.BooleanField(default=False)
    pro_add = serializers.BooleanField(default=False)
    pro_edit = serializers.BooleanField(default=False)
    pro_export = serializers.BooleanField(default=False)
    pro_erase = serializers.BooleanField(default=False)
    pro_import = serializers.BooleanField(default=False)
    pro_download = serializers.BooleanField(default=False)


    class Meta:
        model = Modules
        fields = ('name_profile_modules', 'module', 'module_name', 'mod_consult', 'pro_consul', 'mod_add', 'pro_add',
                  'mod_edit', 'pro_edit', 'mod_export', 'pro_export', 'mod_erase', 'pro_erase', 'mod_import',
                  'pro_import', 'mod_download', 'pro_download')

