from __future__ import unicode_literals
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
from modules.models import Modules
# from modules.serializers import ModuleSerializer
from profiles.models import Profiles, ProfileModules
from profiles.serializers import ProfileSerializer, ProfileModuleSerializer, ModuleSerializer
from AdminBackend.common import Profile, ProfileModule
from security.permissions import IsAuthenticatedActive, HasAccessModuleFunc


class ProfileSolo(viewsets.ModelViewSet):

    """        
        @api {get} /profiles/:id Obtener un Perfil por su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName RetrieveProfileId
        @apiGroup Profile
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiSuccess (200) {String} response OK
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok
            {
                "id_profile": 1,
                "name_profile": "sysadmin",
                "description": "administrar las funcionalidades de los modulos base del sistema",
                "status_id": 1
            }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                "detail": "No encontrado."
            }
    """
    """
        @api {get} /profiles/:id/detail Obtener detalles de un Pefil por su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName DetailProfileId
        @apiGroup Profile
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc 
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiSuccess (200) {String} response OK
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok
            {
                "id_profile": 1,
                "name_profile": "sysadmin",
                "description": "administrar las funcionalidades de los modulos base del sistema",
                "status_id": 1,
                "admin_create": "admin",
                "admin_update": "admin"
            }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                "detail": "No encontrado."
            }
    """
    """
        @api {put} /profiles/:id Actualizar un perfil por su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName UpdateProfileId
        @apiGroup Profile
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParamExample {Json} Request-Update:
            {
                "name_profile": "sysadmin_dev",
                "description": "administrar las funcionalidades de los modulos del sistema dev",
                "status_id": 1
            }
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiParam {String} name_profile - Nombre del perfil, si se cambia debe ser unico, sino se debe incluir igualmente, ya que es requerido.
        @apiParam {String} description - Descripcion del perfil. Si se incluye debe contener 1 o mÃ¡s caracteres. No es requerido.
        @apiParam {Number} status_id - Estatus de perfil. 1: Activo; 2: Inactivo. Su valor por default es 1. No es requerido.
        @apiSuccess (202) {String} response Accepted
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 202 Accepted
            {
                "id_profile": 2,
                "name_profile": "sysadmin_dev",
                "description": "administrar las funcionalidades de los modulos del sistema dev",
                "status_id": 1
            }
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos, etc.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }       
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "name_profile": [
                        "profiles con esta name profile ya existe."
                ]
            }			
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "name_profile": [
                        "Este campo es requerido."
                ]
            }			 
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "description": [
                        "Este campo no puede estar en blanco."
                ]
            }
    """
    """
        @api {put} /profiles/:id/status Actualizar estatus del perfil.       
        @apiHeader {String} Token Users unique access-key.
        @apiName StatusProfileId
        @apiGroup Profile
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParamExample {Json} Request-Status:
            {
                "status_id": 2
            }
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiParam {Number} status_id - Estatus de perfil. 1: Activo; 2: Inactivo. Su valor por default es 1. Es requerido.       
        @apiSuccess (202) {String} response Accepted
        @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 202 Accepted
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos son requeridos o tienen valores no validos etc.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "status_id": [
                    "Este campo es requerido."
                ]
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "status_id": [
                    "Introduzca un numero entero valido."
                ]
            }       
    """
    """
        @api {delete} /profiles/:id Eliminar el perfil dado su id.
        @apiHeader {String} Token Users unique access-key.
        @apiName DeleteProfileId
        @apiGroup Profile
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiSuccess (204) {String} response No Content
        @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 204 No Content
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
    """

    module = 'profiles'
    map_action = {'retrieve': 'pro_consul', 'detail': 'pro_consul', 'update': 'pro_edit', 'status': 'pro_edit',
                  'destroy': 'pro_erase'}
    queryset = None
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def retrieve(self, request, id):
        """Permite obtener un perfil dado su id"""
        instance = Profile.get_object(id)
        queryset = Profiles.objects.get(id_profile=instance.id_profile)
        serializer = self.get_serializer(queryset, fields=('id_profile', 'name_profile', 'description', 'status_id'),
                                         many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def detail(self, request, id):
        """Permite obtener detalles del perfil dado su id"""
        instance = Profile.get_object(id)
        queryset = Profiles.objects.get(id_profile=instance.id_profile)
        serializer = self.get_serializer(queryset, fields=('id_profile', 'name_profile', 'description', 'status_id',
                                                           'admin_create', 'admin_update'), many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, id):
        """Permite actualizar un perfil dado su id"""
        instance = Profile.get_object(id)
        serializer = self.get_serializer(instance, fields=('id_profile', 'name_profile', 'description', 'status_id'),
                                         data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def perform_update(self, serializer, request):
        serializer.save(
            admin_id_update=request.user,
            modified_date=timezone.now()
        )

    def status(self, request, id):
        """Permite solo actualizar el status del perfil a activo/inactivo"""
        try:
            status_id = request.data["status_id"]
        except Exception as e:
            return Response({"status_id": ["Este campo es requerido."]}, status=status.HTTP_400_BAD_REQUEST)
        instance = Profile.get_object(id)
        serializer = self.get_serializer(instance, fields=('status_id',), data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, request)
        return Response(status=status.HTTP_202_ACCEPTED)

    def destroy(self, request, id):
        """Permite borrar de manera logica al perfil dado su id"""
        instance = Profile.get_object(id)
        Profiles.objects.filter(id_profile=instance.id_profile).update(
            status_id=0,
            admin_id_update=request.user,
            modified_date=timezone.now()
        )
        return Response(status=status.HTTP_202_ACCEPTED)


class ProfileList(viewsets.ModelViewSet):
    """
         @api {get} /profiles Listar los perfiles registrados.
         @apiHeader {String} Token User unique access-key.
         @apiName ListProfiles
         @apiGroup ProfileList
         @apiVersion 1.0.0
         @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
         @apiSuccess (200) {String} response Listado de Perfiles registrados.
         @apiSuccessExample {Json} Success-Response:
              HTTP/1.1 200 Ok
              [
                  {
                        "id_profile": 2,
                        "name_profile": "sysadmin_dev",
                        "description": "administrar las funcionalidades de los modulos del sistema dev",
                        "status_id": 1,
                        "admin_create": "admin",
                        "admin_update": "admin"
                  },
                  {
                        "id_profile": 1,
                        "name_profile": "sysadmin",
                        "description": "administrar las funcionalidades de los modulos base del sistema",
                        "status_id": 1,
                        "admin_create": "admin",
                        "admin_update": "admin"
                  }
              ]
    """
    """ 
        @api {post} /profiles Registrar Perfil.
        @apiParamExample {Json} Request-Create:
              {
                  "name_profile": "sysadmin_dev",
                  "description": "administrar las funcionalidades de los modulos base sistema dev"
              }
        @apiHeader {String} Token User unique access-key.
        @apiName CreateProfile
        @apiGroup ProfileList
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {String} name_profile - Nombre del perfil, debe ser Ãºnico. Es requerido.
        @apiParam {String} description - DescripciÃ³n del perfil. Si se incluye debe tener 1 o mÃ¡s caracteres o tener valor null. No es requerido.
        @apiSuccess (201) {String} response Created.
        @apiSuccessExample {string} Success-Response:
             HTTP/1.1 201 Created
             {
                "id": 3
             }
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
        @apiErrorExample {Json} Error-Response 400:
             HTTP/1.1 400 Bad Request
             {
                "name_profile": [
                    "profiles con esta name profile ya existe."
                ]                
             }            
        @apiError (400) {Json} errors
        @apiErrorExample {Json} Error-Response 400:        
             HTTP/1.1 400 Bad Request
             {
                {
                    "name_profile": [
                        "Este campo es requerido."
                    ]
                }
             }
        @apiErrorExample {Json} Error-Response 400:        
             HTTP/1.1 400 Bad Request
             {
                {
                    "description": [
                        "Este campo no puede estar en blanco."
                    ]
                }
             }           
    """

    module = 'profiles'
    map_action = {'list': 'pro_consul', 'create': 'pro_add'}
    queryset = Profiles.objects.filter(status_id__gte=1).order_by('-modified_date')
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def list(self, request, *args, **kwargs):
        """Permite obtener la lista de todos los perfiles"""
        queryset = self.filter_queryset(self.get_queryset())
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #      serializer = self.get_serializer(page, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'date_joined'), many=True)
        #      return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, fields=('id_profile', 'name_profile', 'description', 'status_id',
                                                           'admin_create', 'admin_update'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Permite crear un perfil"""
        serializer = self.get_serializer(data=request.data, fields=('id_profile', 'name_profile', 'description'))
        serializer.is_valid(raise_exception=True)
        profile = self.perform_create(serializer, request)
        return Response({'id': profile.id_profile}, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer, request):
        return serializer.save(
            status_id=1,
            admin_id_create=request.user,
            admin_id_update=request.user
        )


class ProfileModuleList(viewsets.ModelViewSet):
    """
         @api {get} /profiles/:id/modules/in Listar los profilemodules asociados al perfil por su id.
         @apiHeader {String} Token User unique access-key.
         @apiName ListProfileModulesIn
         @apiGroup ProfileModule
         @apiVersion 1.0.0
         @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
         @apiSuccess (200) {String} response Listado de ProfileModules asociados al perfil.
         @apiSuccessExample {Json} Success-Response:
              HTTP/1.1 200 Ok
              [
                  {
                        "name_profile_modules": "admin_users",
                        "module": 1,
                        "module_name": "users",
                        "mod_consult": true,
                        "pro_consul": true,
                        "mod_add": true,
                        "pro_add": true,
                        "mod_edit": true,
                        "pro_edit": true,
                        "mod_export": false,
                        "pro_export": false,
                        "mod_erase": true,
                        "pro_erase": true,
                        "mod_import": false,
                        "pro_import": false,
                        "mod_download": true,
                        "pro_download": true
                  },
                  {
                        "name_profile_modules": "admin_groups",
                        "module": 2,
                        "module_name": "groups",
                        "mod_consult": true,
                        "pro_consul": true,
                        "mod_add": true,
                        "pro_add": true,
                        "mod_edit": true,
                        "pro_edit": true,
                        "mod_export": false,
                        "pro_export": false,
                        "mod_erase": true,
                        "pro_erase": true,
                        "mod_import": false,
                        "pro_import": false,
                        "mod_download": true,
                        "pro_download": true
                  }

              ]
         @apiError (404) {Json} errors Indicando que Id no existe en la bd.
         @apiErrorExample {Json} Error-Response 404:
             HTTP/1.1 404 Not Found
             {
                {
                  "detail": "No encontrado."
                }
             }
    """
    """
         @api {get} /profiles/:id/modules/out Listar los profilemodules no asociados al perfil por su id.
         @apiHeader {String} Token User unique access-key.
         @apiName ListProfileModulesOut
         @apiGroup ProfileModule
         @apiVersion 1.0.0
         @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
         @apiSuccess (200) {String} response Listado de ProfileModules no asociados al perfil.
         @apiSuccessExample {Json} Success-Response:
              HTTP/1.1 200 Ok			     
              [
                  {
                        "name_profile_modules": "",
                        "module": 4,
                        "module_name": "profiles",
                        "mod_consult": true,
                        "pro_consul": false,
                        "mod_add": true,
                        "pro_add": false,
                        "mod_edit": true,
                        "pro_edit": false,
                        "mod_export": false,
                        "pro_export": false,
                        "mod_erase": true,
                        "pro_erase": false,
                        "mod_import": false,
                        "pro_import": false,
                        "mod_download": true,
                        "pro_download": false
                  },
                  {
                        "name_profile_modules": "",
                        "module": 3,
                        "module_name": "modules",
                        "mod_consult": true,
                        "pro_consul": false,
                        "mod_add": true,
                        "pro_add": false,
                        "mod_edit": true,
                        "pro_edit": false,
                        "mod_export": false,
                        "pro_export": false,
                        "mod_erase": true,
                        "pro_erase": false,
                        "mod_import": false,
                        "pro_import": false,
                        "mod_download": true,
                        "pro_download": false
                  } 

              ]
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiErrorExample {Json} Error-Response 404:
             HTTP/1.1 404 Not Found
             {
                {
                  "detail": "No encontrado."
                }
             }
    """
    """ 
        @api {post} /profiles/:id/modules Registrar por lote los profilemodules de un perfil dado su id.
        @apiParamExample {Json} Request-Create:              
             {
                  "profilemodules": [
                      {
                            "name_profile_modules": "admin_users",
                            "module": 1,													
                            "pro_consul": true,													
                            "pro_add": true,													
                            "pro_edit": false,													
                            "pro_export": true,													
                            "pro_erase": false,													
                            "pro_import": true,													
                            "pro_download": false
                       },
                       {
                            "name_profile_modules": "admin_groups",
                            "module": 2,
                            "pro_consul": true,													
                            "pro_add": true,													
                            "pro_edit": false,													
                            "pro_export": true,													
                            "pro_erase": false,													
                            "pro_import": true,													
                            "pro_download": false
                       }]
             }        
        @apiHeader {String} Token User unique access-key.
        @apiName CreateProfileModules
        @apiGroup ProfileModule
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiParam {Json[]} profilemodules - Arreglo de json. Cada json contiene los datos del profilemodule a registrar.
        @apiParam {String} name_profile_modules - Nombre del profilemodule, debe ser unico. Es requerido.
        @apiParam {Number} module - Id de un modulo existente, al cual se va a asociar el perfil en el profilemodule a registrar. Es requerido.
        @apiParam {Boolean} pro_consul - Indica si tiene permiso para operaciones de consulta. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_add - Indica si tiene permiso para operaciones de agregar registros. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_edit - Indica si tiene permiso para operaciones de edicion. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_export - Indica si tiene permiso para operaciones de exportacion de contenido. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_erase - Indica si tiene permiso para operaciones de borrado. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_import - Indica si tiene permiso para operaciones de importacion de contenido. Su valor por default es false. No es requerido.
        @apiParam {Boolean} pro_download - Indica si tiene permiso para operaciones de descarga de contenido. Su valor por default es false. No es requerido.
        @apiSuccess (201) {String} response Created.
        @apiSuccessExample {string} Success-Response:
             HTTP/1.1 201 Created
             []
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
        @apiErrorExample {Json} Error-Response 404:
             HTTP/1.1 404 Not Found
             {
                {
                  "detail": "No encontrado."
                }
             }
        @apiErrorExample {Json} Error-Response 400:
             HTTP/1.1 400 Bad Request
             {
                {
                    "name_profile_modules": [
                        "profile modules con esta name profile modules ya existe."
                    ]
                }
             }
        @apiErrorExample {Json} Error-Response 400:
             HTTP/1.1 400 Bad Request
             {
                {
                    "name_profile_modules": [
                        "Este campo es requerido."
                    ]
                }
             }
        @apiErrorExample {Json} Error-Response 400:        
             HTTP/1.1 400 Bad Request
             {
                {
                    "module": [
                        "Este campo es requerido."
                    ]
                }
             }
        @apiErrorExample {Json} Error-Response 400:        
             HTTP/1.1 400 Bad Request
             {
                {
                    "module": [
                        "Clave primaria \"9\" invalida - objeto no existe."
                    ]
                }
             }           
    """
    """ 
        @api {put} /profiles/:id/modules Actualizar por lote los profilemodules de un perfil dado su id.
        @apiParamExample {Json} Request-Create:              
             {
                  "profilemodules": [
                      {
                            "name_profile_modules": "admin_users",
                            "module": 1,													
                            "pro_consul": true,													
                            "pro_add": true,													
                            "pro_edit": false,													
                            "pro_export": true,													
                            "pro_erase": false,													
                            "pro_import": true,													
                            "pro_download": false
                      },
                      {
                            "name_profile_modules": "admin_groups",
                            "module": 2,
                            "pro_consul": true,													
                            "pro_add": true,													
                            "pro_edit": false,													
                            "pro_export": true,													
                            "pro_erase": false,													
                            "pro_import": true,													
                            "pro_download": false
                      }]
             }        
        @apiHeader {String} Token User unique access-key.
        @apiName UpdateProfileModules
        @apiGroup ProfileModule
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {Number} id - id del Perfil en la url del request.
        @apiParam {Json[]} profilemodules - Arreglo de json. Cada json contiene los datos del profilemodule a actualizar.
        @apiParam {String} name_profile_modules - Nombre del profilemodule, si se cambia debe ser unico, sino se debe incluir igualmente, ya que es requerido.
        @apiParam {Number} module - Id del modulo existente al cual esta asociado el perfil en el profilemodule a actualizar. Es requerido.
        @apiParam {Boolean} pro_consul - Indica si tiene permiso para operaciones de consulta de registros. No es requerido.
        @apiParam {Boolean} pro_add - Indica si tiene permiso para operaciones de agregar registros. No es requerido.
        @apiParam {Boolean} pro_edit - Indica si tiene permiso para operaciones de edicion de registros. No es requerido.
        @apiParam {Boolean} pro_export - Indica si tiene permiso para operaciones de exportaciÃ³n de contenido. No es requerido.
        @apiParam {Boolean} pro_erase - Indica si tiene permiso para operaciones de borrado de registros. No es requerido.
        @apiParam {Boolean} pro_import - Indica si tiene permiso para operaciones de importacion de contenido. No es requerido.
        @apiParam {Boolean} pro_download - Indica si tiene permiso para operaciones de descarga de contenido. No es requerido.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
        @apiError (404) {Json} errors Indicando que Id no existe en la bd.
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                    "name_profile_modules": [
                        "profile modules con esta name profile modules ya existe."
                    ]
                }
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                    "name_profile_modules": [
                        "Este campo es requerido."
                    ]
                }
            }
        @apiErrorExample {Json} Error-Response 400:        
            HTTP/1.1 400 Bad Request
            {
                {
                    "module": [
                        "Este campo es requerido."
                    ]
                }
            }           
    """
    """
       @api {delete} /profiles/:id/modules Eliminar por lote los profilemodules de un perfil dado su id.
       @apiParamExample {Json} Request-Create:              
              {
                  "profilemodules": [1,2]
              }
       @apiHeader {String} Token Users unique access-key.
       @apiName DeleteProfileModules
       @apiGroup ProfileModule
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
       @apiParam {Number} id - id del Perfil en la url del request.
       @apiParam {Number[]} profilemodules - Arreglo de ids. Cada id corresponde a un modulo asociado al perfil en los profilemodules a borrar.
       @apiSuccess (204) {String} response No Content
       @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 204 No Content
       @apiError (404) {Json} errors Indicando que Id no existe en la bd.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
    """

    module = 'profiles'
    map_action = {'list_modules_in': 'pro_consul', 'list_modules_out': 'pro_consul', 'update': 'pro_edit',
                  'create': 'pro_add', 'destroy': 'pro_erase'}
    queryset = None
    serializer_class = ProfileModuleSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def list_modules_in(self, request, id):
        """Permite obtener los modulos asociados al perfil dado su id, incluyendo existencia de funciones y permisos"""
        self.serializer_class = ProfileModuleSerializer
        instance = Profile.get_object(id)
        self.queryset = ProfileModules.objects.filter(profile=instance.id_profile)
        serializer = self.get_serializer(self.queryset, fields=(
            'name_profile_modules', 'module', 'module_name', 'mod_consult', 'pro_consul', 'mod_add', 'pro_add',
            'mod_edit', 'pro_edit', 'mod_export', 'pro_export', 'mod_erase', 'pro_erase', 'mod_import',
            'pro_import', 'mod_download', 'pro_download'), many=True)
        return Response(serializer.data)

    def list_modules_out(self, request, id):
        """Permite obtener los modulos no asociados al perfil dado su id"""
        instance = Profile.get_object(id)
        self.serializer_class = ModuleSerializer
        self.queryset = Modules.objects.exclude(
            id_module__in=ProfileModules.objects.filter(profile_id=instance.id_profile)
            .values('module_id'))
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(serializer.data)

    def update(self, request, id, *args, **kwargs):
        """Permite actualizar un lote de modulos asociados a un perfil dado su id, especificando los acceso al modulo en su respectivo profile_module"""
        self.serializer_class = ProfileModuleSerializer
        response = dict()
        response['errors'] = []
        instance = Profile.get_object_active(id)
        try:
            profilemodules = request.data["profilemodules"]
            for profilemodule in profilemodules:
                instance_profilemodule = None
                module = None
                try:
                    module = int(profilemodule['module'])
                    instance_profilemodule = ProfileModule.get_object(profile=instance.id_profile, module=module)
                except Exception as e:
                    pass
                if instance_profilemodule:
                    serializer = self.get_serializer(instance_profilemodule, data=profilemodule, fields=(
                        'id_profile_module', 'name_profile_modules', 'module', 'pro_consul', 'pro_add',
                        'pro_edit', 'pro_export', 'pro_erase', 'pro_import', 'pro_download'
                    ))
                    try:
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            response['errors'].append(serializer.errors)
                    except Exception as e:
                        response['errors'].append(serializer.errors)
            status_response = status.HTTP_202_ACCEPTED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserializar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, id, *args, **kwargs):
        """Permite asociar un lote de modulos a un perfil dado su id, especificando los acceso al modulo en su respectivo profile_modules
        Igualmente permite actualizar los accesos de las asociaciones profilemodules que ya existan"""
        self.serializer_class = ProfileModuleSerializer
        response = dict()
        response['errors'] = []
        instance = Profile.get_object_active(id)
        try:
            profilemodules = request.data["profilemodules"]
            for profilemodule in profilemodules:
                instance_profilemodule = None
                module = None
                try:
                    module = int(profilemodule['module'])
                    instance_profilemodule = ProfileModule.get_object(profile=instance.id_profile, module=module)
                except Exception as e:
                    pass
                if instance_profilemodule:
                    serializer = self.get_serializer(instance_profilemodule, data=profilemodule, fields=(
                        'id_profile_module', 'name_profile_modules', 'module', 'pro_consul', 'pro_add',
                        'pro_edit', 'pro_export', 'pro_erase', 'pro_import', 'pro_download'
                    ))
                else:
                    serializer = self.get_serializer(data=profilemodule, fields=(
                        'id_profile_module', 'name_profile_modules', 'module', 'pro_consul', 'pro_add',
                        'pro_edit', 'pro_export', 'pro_erase', 'pro_import', 'pro_download'))
                try:
                    if serializer.is_valid():
                        serializer.save(profile=instance)
                    else:
                        response['errors'].append(serializer.errors)
                except Exception as e:
                    response['errors'].append(serializer.errors)
            status_response = status.HTTP_201_CREATED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserializar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, id):
        """Permite borrar un lote de perfiles de un grupo dado su id"""
        response = dict()
        response['errors'] = []
        instance = Profile.get_object_active(id)
        try:
            profilemodules = request.data["profilemodules"]
            for profilemodule in profilemodules:
                try:
                    ProfileModules.objects.filter(profile=instance.id_profile, module=profilemodule).delete()
                except Exception as e:
                    response['errors'].append({"error": ["No se pudo realizar la operacion."]})
            status_response = status.HTTP_204_NO_CONTENT if not len(
                response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserializar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

