from django.conf.urls import url
from profiles import views


profile_list = views.ProfileList.as_view({
    'get': 'list',
    'post': 'create'
})

profile_solo = views.ProfileSolo.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

profile_detail = views.ProfileSolo.as_view({
    'get': 'detail'
})

profile_status = views.ProfileSolo.as_view({
    'put': 'status'
})

profilemodules = views.ProfileModuleList.as_view({
    'post': 'create',
    'put': 'update',
    'delete': 'destroy'
})

profile_modules_in = views.ProfileModuleList.as_view({
    'get': 'list_modules_in',
})

profile_modules_out = views.ProfileModuleList.as_view({
    'get': 'list_modules_out',
})

urlpatterns = [
    url(r'^profiles$', profile_list, name='groups-list'),
    url(r'^profiles/(?P<id>[0-9]+)$', profile_solo, name='group-solo'),
    url(r'^profiles/(?P<id>[0-9]+)/detail$', profile_detail, name='group-detail'),
    url(r'^profiles/(?P<id>[0-9]+)/status$', profile_status, name='group-status'),

    #ProfilesModules
    url(r'^profiles/(?P<id>[0-9]+)/modules$', profilemodules, name='profilemodules'),
    url(r'^profiles/(?P<id>[0-9]+)/modules/in$', profile_modules_in, name='profile_modules'),
    url(r'^profiles/(?P<id>[0-9]+)/modules/out$', profile_modules_out, name='profile_modules'),


]


