# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone

from users.models import Admin
from modules.models import Modules

# Create your models here.


class Profiles(models.Model):
    id_profile = models.AutoField(primary_key=True)
    name_profile = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=120, null=True)
    status_id = models.IntegerField(default=1, validators=[MaxValueValidator(2), MinValueValidator(0)])
    create_date = models.DateTimeField(default=timezone.now)
    modified_date = models.DateTimeField(default=timezone.now)
    admin_id_create = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='admincreateprofile')
    admin_id_update = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='adminupdateprofile')

    class Meta:
        db_table = 'profile'

    def __str__(self):
        return self.id_profile


class ProfileAdmin(models.Model):
    id_profile_admin = models.AutoField(primary_key=True)
    profile = models.ForeignKey(Profiles, on_delete=models.CASCADE, related_name='profile_admin_profile')
    admin = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='profile_admin_admin')

    class Meta:
         db_table = 'profile_admin'
         unique_together = ('profile', 'admin')

    def __str__(self):
        return self.id_profile_admin


class ProfileModules(models.Model):
    id_profile_module = models.AutoField(primary_key=True)
    name_profile_modules = models.CharField(max_length=50, unique=True)
    profile = models.ForeignKey(Profiles, on_delete=models.CASCADE, related_name='profile_module_profile')
    module = models.ForeignKey(Modules, on_delete=models.CASCADE, related_name='profile_module_module')
    pro_consul = models.BooleanField(default=False)
    pro_add = models.BooleanField(default=False)
    pro_edit = models.BooleanField(default=False)
    pro_export = models.BooleanField(default=False)
    pro_erase = models.BooleanField(default=False)
    pro_import = models.BooleanField(default=False)
    pro_download = models.BooleanField(default=False)

    class Meta:
        db_table = 'profile_modules'
        unique_together = ('profile', 'module')

    def __str__(self):
        return self.id_profile_module


