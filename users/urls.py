from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url
from users import views


urlpatterns = [
    url(r'^users$', views.UserList.as_view({'get': 'list','post': 'create'}), name='user-list'),
    url(r'^users/(?P<id>[0-9]+)$', views.UserDetail.as_view({'get': 'retrieve','put': 'update','delete': 'destroy'}), name='user-detail'),
    url(r'^users/(?P<id>[0-9]+)/status$', views.UserDetail.as_view({'put': 'status'}), name='user-status'),
    url(r'^users/(?P<id>[0-9]+)/change_password$', views.UserDetail.as_view({'put': 'change_password'}), name='user-change-password'),
    url(r'^users/(?P<id>[0-9]+)/generation_password', views.UserDetail.as_view({'get': 'generation_password'}), name='user-generation-password'),
    url(r'^users/(?P<token>[\w.@+-]+)/assign_password$', views.UserPassword.as_view({'put': 'assign_password'}), name='user-assign-password'),

    #UsersGroups
    url(r'^users/(?P<id>[0-9]+)/groups/in$', views.UserGroups.as_view({'get': 'list_groups_in'}), name='user-groups-in'),
    url(r'^users/(?P<id>[0-9]+)/groups/out$', views.UserGroups.as_view({'get': 'list_groups_out'}), name='user-groups-out'),
    url(r'^users/(?P<id>[0-9]+)/groups$', views.UserGroups.as_view({'post': 'create', 'delete': 'destroy'}), name='users-groups'),

    #UsersProfile
    url(r'^users/(?P<id>[0-9]+)/profiles/in$', views.UserProfiles.as_view({'get': 'list_profiles_in'}), name='user-profiles-in'),
    url(r'^users/(?P<id>[0-9]+)/profiles/out$',  views.UserProfiles.as_view({'get': 'list_profiles_out'}), name='user-profiles-out'),
    url(r'^users/(?P<id>[0-9]+)/profiles$', views.UserProfiles.as_view({'post': 'create', 'delete': 'destroy'}), name='users-profiles'),

#UsersModules
    url(r'^users/(?P<id>[0-9]+)/modules$', views.UserModules.as_view({'get': 'list_modules_user'}),name='users-modules'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
