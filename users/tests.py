# -*- coding: utf-8 -*-
"""
ES NECESARIO MANTENER REDIS ACTIVADO YA QUE LAS VARIABLES DE SESION SON ALMACENADAS EN REDIS
"""
from __future__ import unicode_literals
from users.models import Admin
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient, RequestsClient
from django.core.urlresolvers import reverse
# Create your tests here.


class GetTestPage(TestCase):
    """Test de pruebas para peticiones genericas."""

    def setUp(self):
        self.client = RequestsClient()
        pass

    def test_get_page_login(self):
        """Devuelve un 405 debido a que esta reuqiere parametros"""
        self.client = APIClient()
        response = self.client.get(
            reverse('login'))
        self.assertEqual(response.status_code, 405)

    def test_get_page_load_logout(self):
        """Comprobamos la peticion a la vista logout por get."""
        self.client = APIClient()
        response = self.client.get(
            reverse('logout'))
        self.assertEqual(response.status_code, 401)


class UserTest(TestCase):
    """Tests para peticiones relacionadas al login."""

    def setUp(self):
        """Definimos un usuario."""
        user = Admin.objects.create_user(
            'test', password='testing', email='test@gmail.com')
        user.is_superuser = True
        user.is_staff = True
        user.save()

    def test_login_admin(self):
        self.client = APIClient()
        response = self.client.post(
            '/login',
            {'username': 'test', 'password': 'testing'}, format='json')
        self.assertEqual(response.status_code, 202)

    def test_security_login_admin_incorrect(self):
        """Test que rechaza las credenciales si son invalidas."""
        self.client = APIClient()
        response = self.client.post(
            '/login',
            {'username': 'test', 'password': 'wrong'}, format='json')
        self.assertEqual(response.status_code, 401)

    def test_security_logout_admin_incorrect(self):
        """No admite desloguearse sin credenciales"""
        self.client = APIClient()
        response = self.client.post(
            '/logout',
            {'token': 'sdfsdfsadfsadff'}, format='json')
        self.assertEqual(response.status_code, 401)


class ReadUserTest(TestCase):
    """Tests para la verificacion de usuarios"""

    def setUp(self):
        """Definimos un usuario."""
        user = Admin.objects.create_user(
            'test1', password='testing1', email='test1@gmail.com')
        user.is_superuser = True
        user.is_staff = True
        user.save()

    def test_can_read_user_list(self):
        """Test para verificar la lista de usuarios."""
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, 200)

    def test_can_read_user_detail(self):
        """Test para ver los detalles de un usuario."""
        response = self.client.get(
            reverse('user-detail', kwargs={'id': 1}), format='json')
        self.assertEqual(response.status_code, 200)


class UserDeleteTestCase(TestCase):
    """Actualizacion y datos de usuario."""

    def setUp(self):
        """Definimos un usuario."""
        user = Admin.objects.create_user(
            'test1', password='testing1', email='test1@gmail.com')
        user.is_superuser = True
        user.is_staff = True
        user.save()

    def test_can_delete_user(self):
        """Test que permite la eliminacion de usuarios"""
        response = self.client.delete(reverse(
            'user-detail', kwargs={'id': 1}), format='json')
        self.assertEqual(response.status_code, 202)