from django.utils import timezone
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from security.permissions import IsAuthenticatedActive, HasAccessModuleFunc
from users.models import Admin
from groups.models import Groups, GroupAdmin
from modules.models import Modules
from profiles.models import Profiles, ProfileAdmin, ProfileModules
from users.serializers import UserSerializer, GroupAdminSerializer, ProfilesMinSerializer, ProfileAdminSerializer, \
    ModuleMinSerializer, UsersPassword
from groups.serializers import GroupSerializer
from rest_framework import viewsets
from AdminBackend.common import Users, Group, Profile
from AdminBackend.common.gen_password import Gen_Password
from AdminBackend.common.email import Send_Email
from AdminBackend.common.token_itsdangerous import Token_Itsdangerous
from AdminBackend.settings import URL_VERIFY



################## LIST Y CREATE USER ######################

class UserList(viewsets.ModelViewSet):
    """
       @api {get} /users Listar Usuarios Registrados(Activo/Inactivo)
       @apiHeader {String} Token User unique access-key.
       @apiName GetUser
       @apiGroup User
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiSuccess (200) {String} response Listado de Usuarios registrados.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
               [
                    {
                        "id_admin": 1,
                        "first_name": "admin",
                        "last_name": "admin",
                        "username": "admin",
                        "email": "admin@brightquest.tech",
                        "date_joined": "2017-07-31T21:12:27.783835Z",
                        "is_active": 1
                    }
                ]
           }
    """
    """ 
        @api {post} /users Registrar Usuario
        @apiParamExample {Json} Request-Create:
            {
                "first_name": "admin",
                "last_name": "admin",
                "username": "admin",
                "email": "admin@brightquest.tech"
            }
        @apiHeader {String} Token User unique access-key.
        @apiName PostUser
        @apiGroup User
        @apiVersion 1.0.0
        @apiPermission IsAuthenticated
        @apiParam {String} first_name - Nombre del usuario
        @apiParam {String} last_name - Apellido del usuario
        @apiParam {String} username - Nombre de pila del usuario
        @apiParam {Email} email - Correo del usuario

        @apiSuccess (201) {String} response Created.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 201 Created
            {
                {
                    "message": "Se ha enviado un link a su correo para asignar su contrasena y completar el registro!"
                }
            }
            
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "email": [
                    "admin con esta email ya existe."
                ],
                "username": [
                    "Ya existe un usuario con este nombre."
                ]
            }
            HTTP/1.1 400 Bad Request
            {
               {
                    "email": [
                        "admin con esta email ya existe."
                    ]
               }
            }
            
            HTTP/1.1 400 Bad Request
            {
               {
                    "username": [
                        "Ya existe un usuario con este nombre."
                    ]
               }
            }
        @apiError (400) {Json} errors
        @apiErrorExample {Json} Error-Response 400:        
            HTTP/1.1 400 Bad Request
            {
               {
                    "username": [
                        "Este campo es requerido."
                    ],
                    "email": [
                        "Este campo es requerido."
                    ]
                }
            }           
    """

    module = 'users'
    map_action = {'list': 'pro_consul', 'create': 'pro_add'}
    queryset = Admin.objects.filter(is_active__gt=0).order_by('date_joined')
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)


    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #      serializer = self.get_serializer(page, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'date_joined'), many=True)
        #      return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'is_active', 'date_joined'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, fields=('id_admin','first_name', 'last_name', 'username', 'email'))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer, request)
        return Response({'message': "Se ha enviado un link a su correo para asiganar su contrasena y completar el registro!"}, status=status.HTTP_201_CREATED)


    def perform_create(self, serializer, request):
        password = Gen_Password.password()
        serializer.save(
                    password=password,
                    admin_id_create=request.user,
                    admin_id_update=request.user)
        user = Admin.objects.filter(id_admin=serializer.data['id_admin']).values('id_admin','username','email')
        data = Token_Itsdangerous.generated_token(user[0]['id_admin'])
        token = URL_VERIFY + data
        Send_Email.email(user[0]['email'], password, token)

################## DELETE AND UPDATE OF USER ######################
class UserDetail(viewsets.ModelViewSet):
    """
       @api {get} /users/:id Listar Usuario por su id.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserId
       @apiGroup UserDetail
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {String} response OK
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
               {
                "id_admin": 1,
                "first_name": "admin",
                "last_name": "admin",
                "username": "admin",
                "email": "admin@brightquest.tech",
                "date_joined": "2017-07-31T21:12:27.783835Z",
                "is_active": 1
                }
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
    """
    """
       @api {put} /users/:id Actualizar usuario por su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName PutUserId
       @apiGroup UserDetail
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
           {
               {
                   "first_name": "admin",
                   "last_name": "admin",
                   "username": "admin",
                   "email": "admin@brightquest.tech",
                   "admin_id_update": 1
                }
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
       @apiError (400) {Json} errors Indicando que username/email pertenece a otro usuario.
       @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                    "username": [
                        "Ya existe un usuario con este nombre."
                    ]
                }
            }
            HTTP/1.1 400 Bad Request
            {
                {
                    "email": [
                        "admin con esta email ya existe."
                    ]
                }
            }
    """
    """
       @api {delete} /users/:id Elimina usuario por su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName DeleteUserId
       @apiGroup UserDetail
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }
    """
    """
       @api {put} /users/:id/status Actualiza estatus del usuario.
       @apiParamExample {Json} Request-Status:
            {
                "status": 1 o 2
            }
       @apiHeader {String} Token Users unique access-key.
       @apiName PutUserIdStatus
       @apiGroup UserDetail
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiParam {Number} status - estatus(1 o 2) del Usuario.       
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }       
    """
    """
       @api {put} /users/:id/change_password Users - Cambio de contrasena de usuario.
       @apiParamExample {Json} Request-Change-Password:
           {
               "password": "admin",
               "new_password": "admin"
           }
       @apiHeader {String} Token Users unique access-key.
       @apiName PutUserIdChange
       @apiGroup UserDetail
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.       
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
          HTTP/1.1 202 Accepted
       @apiError (400) {Json} errors Indicando que username/email pertenece a otro usuario.
       @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                    "password": [
                        "Contresena Incorrecta"
                    ]
                }
            }       
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }       
    """
    module = 'users'
    map_action = {'retrieve': 'pro_consul', 'update': 'pro_edit', 'status': 'pro_edit', 'change_password': 'pro_edit',
                  'generation_password': 'pro_edit', 'destroy': 'pro_erase'}
    serializer_class = UserSerializer
    queryset = None
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def retrieve(self, request, id):
        instance = Users.get_object(id)
        queryset = Admin.objects.get(id_admin=instance.id_admin)
        serializer = self.get_serializer(queryset, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'date_joined', 'is_active'), many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, **kwargs)

    def update(self, request, id, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = Users.get_object(id)
        serializer = self.get_serializer(instance, data=request.data, fields=('first_name', 'last_name', 'username','email', 'admin_id_update'), partial = partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer,request)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    def perform_update(self, serializer,request):
        serializer.save(admin_id_update=request.user, modified_date=timezone.now())

    def destroy(self, request, id):
        Users.get_object(id)
        Admin.objects.filter(id_admin=id).update(
            is_active=0,
            admin_id_update=request.user,
            modified_date=timezone.now())
        return Response(status=status.HTTP_202_ACCEPTED)

    def status(self, request, id):
        Users.get_object(id)
        Admin.objects.filter(id_admin=id).update(is_active=request.data['status'],
            admin_id_update=request.user,
            modified_date=timezone.now())
        return Response(status=status.HTTP_202_ACCEPTED)

    def change_password(self, request, id):
        instance = Users.get_object(id)
        serializer = self.get_serializer(data=request.data, fields=('password', 'new_password'))
        if serializer.is_valid():
            if instance and instance.check_password(serializer.data['password']):
                instance.set_password(serializer.data.get('new_password'))
                instance.save()
                return Response(status=status.HTTP_202_ACCEPTED)
            return Response({'password': ['Contresena Incorrecta']}, status=status.HTTP_400_BAD_REQUEST)

    def generation_password(self, request, id):
        Users.get_object(id)
        instance = Admin.objects.filter(id_admin=id).values('email')
        password = Gen_Password.password()
        return self.send_email_password(instance[0]['email'], password, id)

    def send_email_password(self, instance, password, id):
        data = Send_Email(instance, password, token=0).email()
        if data.status_code == 200:
            self.partial_update_password(password, id)
        return Response(data.json())

    def partial_update_password(self, password, id):
        instance = Admin.objects.get(id_admin=id)
        instance.set_password(password)
        instance.save()


################## USER ADD GROUPS ######################

class UserGroups(viewsets.ModelViewSet):
    """
       @api {get} /users/:id/groups/in Listar Grupos pertenencientes a Usuario.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserGroupIn
       @apiGroup UserGroups
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {Json} response Listado de Grupos pertenencientes a Usuario.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_group": 1,
                    "name_group": "admin",
                    "status_id": 1
                },
                {
                    "id_group": 2,
                    "name_group": "admin2",
                    "status_id": 1
                }
              ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
       @api {get} /users/:id/groups/out Listar Grupos No pertenencientes a Usuario.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserGroupOut
       @apiGroup UserGroups
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {Json} response Listado de Grupos no pertenencientes a Usuario.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_group": 1,
                    "name_group": "admin",
                    "status_id": 1
                },
                {
                    "id_group": 2,
                    "name_group": "admin2",
                    "status_id": 1
                }
              ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
       @api {post} /users/:id/groups Crear Grupos a Usuario
       @apiParamExample {Json} Request-User-Group:
           {
                "groups": [1,2,...]
           }
       @apiHeader {String} Token User unique access-key.
       @apiName PostUserGroup
       @apiGroup UserGroups
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (201) {Json} response
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 201 Created
           {
                []
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
       @apiError (400) {Json} errors Indicando que clave primaria es invalida.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {
                {
                    "group": [
                        "Clave primaria \":id\" inválida - objeto no existe."
                    ]
                }
           }
       @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """

    """
       @api {delete} /users/:id/groups Eliminar Grupos pertenencientes a Usuario.
       @apiParamExample {Json} Request-User-Group:
           {
                "groups": [1,2,...]
           }
       @apiHeader {String} Token User unique access-key.
       @apiName DeleteUserGroup
       @apiGroup UserGroups
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
       @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """

    module = 'users'
    map_action = {'list_groups_in': 'pro_consul', 'list_groups_out': 'pro_consul', 'create': 'pro_add',
                  'destroy': 'pro_erase'}
    queryset = None
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def list_groups_in(self, request, id):
        Users.get_object(id)
        queryset = Groups.objects.filter(id_group__in=GroupAdmin.objects.filter(admin_id=id).
                                                values('group_id')).filter(status_id__gte=1).order_by('name_group')
        serializer = self.get_serializer(queryset, fields=('id_group', 'name_group', 'status_id'), many=True)
        return Response(serializer.data)

    def list_groups_out(self, request, id):
        Users.get_object(id)
        queryset = Groups.objects.exclude(id_group__in=GroupAdmin.objects.filter(admin_id=id).
                                                values('group_id')).filter(status_id__gte=1).order_by('name_group')
        serializer = self.get_serializer(queryset, fields=('id_group', 'name_group', 'status_id'), many=True)
        return Response(serializer.data)

    def create(self, request, id):
        response = dict()
        response['errors'] = []
        instance = Users.get_object_active(id)
        try:
            groups = request.data["groups"]
            for data in groups:
                groupuser = None
                groupuser = GroupAdmin.objects.filter(group=data, admin=instance.id_admin)
                if not groupuser:
                    serializer = GroupAdminSerializer(data={'group': data, 'admin': instance.id_admin})
                    try:
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            response['errors'].append(serializer.errors)
                    except Exception as e:
                        response['errors'].append(serializer.errors)
            status_response = status.HTTP_201_CREATED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, id):
        response = dict()
        response['errors'] = []
        instance = Users.get_object_active(id)
        try:
            groups = request.data['groups']
            for data in groups:
                try:
                    GroupAdmin.objects.filter(group=data, admin=instance.id_admin).delete()
                except Exception as e:
                    response['errors'].append({"error": ["No se pudo realizar la operacion."]})
            status_response = status.HTTP_202_ACCEPTED if not len(
                response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)


class UserProfiles(viewsets.ModelViewSet):
    """
       @api {get} /users/:id/profiles/in Listar Perfiles pertenencientes a Usuario.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserProfilesIn
       @apiGroup UserProfiles
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {Json} response Listado de Perfiles pertenencientes a Usuario.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
             [
                {
                    "id_profile": 1,
                    "name_profile": "Matematica",
                    "status_id": 1
                }
             ]
           }
           HTTP/1.1 200 Ok
           {
              []
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    """
       @api {get} /users/:id/profiles/out Listar Perfiles No pertenencientes a Usuario.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserProfilesOut
       @apiGroup UserProfiles
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {Json} response Listado de Perfiles no pertenencientes a Usuario.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
              []
           }
           HTTP/1.1 200 Ok
           {
              [
                {
                    "id_profile": 1,
                    "name_profile": "Matematica",
                    "status_id": 1
                }
              ]
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
       @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """
    """
       @api {post} /users/:id/profiles Crear Perfiles a Usuario
       @apiParamExample {Json} Request-User-Profiles:
          {
               {"profiles": [1,2,...]}
          }
       @apiHeader {String} Token User unique access-key.
       @apiName PostUserProfiles
       @apiGroup UserProfiles
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (201) {Json} response
       @apiSuccessExample {Json} Success-Response:
          HTTP/1.1 201 Created
          {
              []
          }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
       @apiError (400) {Json} errors Indicando que clave primaria es invalida.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {
                {
                    "profile": [
                        "Clave primaria \":id\" inválida - objeto no existe."
                    ]
                }
           }
       @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
       
    """

    """
       @api {delete} /users/:id/profiles Eliminar Perfiles pertenencientes a Usuario.
       @apiParamExample {Json} Request-User-Profiles:
           {
                "profiles": [1,2,...]
           }
       @apiHeader {String} Token User unique access-key.
       @apiName DeleteUserProfiles
       @apiGroup UserProfiles
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
       @apiError (400) {Json} errors Indicando que clave primaria es invalida.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {
                {
                    "group": [
                        "Clave primaria \":id\" inválida - objeto no existe."
                    ]
                }
           }
       @apiError (400) {Json} errors Indicando que No se puedo desealizar la data.
       @apiErrorExample {Json} Error-Response 400:
           HTTP/1.1 400 Bad Request
           {    
                {
                    "error": [
                        "No se pudo deserealizar la data."
                    ]
                }
           }
    """

    module = 'users'
    map_action = {'list_profiles_in': 'pro_consul', 'list_profiles_out': 'pro_consul', 'create': 'pro_add',
                  'destroy': 'pro_erase'}
    queryset = None
    serializer_class = ProfilesMinSerializer
    permission_classes = (IsAuthenticatedActive, HasAccessModuleFunc,)

    def list_profiles_in(self, request, id):
        Users.get_object(id)
        queryset = Profiles.objects.filter(id_profile__in=ProfileAdmin.objects.filter(admin_id=id).
                                                values('profile_id')).filter(status_id__gte=1).order_by('name_profile')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def list_profiles_out(self, request, id):
        Users.get_object(id)
        queryset = Profiles.objects.exclude(id_profile__in=ProfileAdmin.objects.filter(admin_id=id).
                                                values('profile_id')).filter(status_id__gte=1).order_by('name_profile')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, id):
        response = dict()
        response['errors'] = []
        instance = Users.get_object_active(id)
        try:
            profiles = request.data["profiles"]
            for data in profiles:
                profileadmin = None
                profileadmin = ProfileAdmin.objects.filter(admin=instance.id_admin, profile=data)
                if not profileadmin:
                    serializer = ProfileAdminSerializer(data={'admin': id, 'profile': data})
                    try:
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            response['errors'].append(serializer.errors)
                    except Exception as e:
                        response['errors'].append(serializer.errors)
            status_response = status.HTTP_201_CREATED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, id):
        response = dict()
        response['errors'] = []
        instance = Users.get_object_active(id)
        try:
            profiles = request.data["profiles"]
            for data in profiles:
                try:
                    ProfileAdmin.objects.filter(admin=instance.id_admin, profile=data).delete()
                except Exception as e:
                    response['errors'].append({"error": ["No se pudo realizar la operacion."]})
            status_response = status.HTTP_202_ACCEPTED if not len(response['errors']) else status.HTTP_400_BAD_REQUEST
            return Response(response['errors'], status=status_response)
        except Exception as e:
            response['errors'].append({"error": ["No se pudo deserealizar la data."]})
            return Response(response['errors'], status=status.HTTP_400_BAD_REQUEST)


class UserModules(viewsets.ModelViewSet):
    """
       @api {get} /users/:id/modules Listar Modulos pertenencientes a Usuario.
       @apiHeader {String} Token User unique access-key.
       @apiName GetUserModules
       @apiGroup UserModules
       @apiVersion 1.0.0
       @apiPermission IsAuthenticated
       @apiParam {Number} id - id del Usuario.
       @apiSuccess (200) {Json} response Listado de Modules pertenencientes a Usuario.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok
           {
                [
                    {
                        "id_module": 4,
                        "name_module": "profiles",
                        "name_menu": "menu",
                        "icon": "build"
                    },
                    {
                        "id_module": 3,
                        "name_module": "modules",
                        "name_menu": "menu1",
                        "icon": "build"
                    },
                    {
                        "id_module": 2,
                        "name_module": "groups",
                        "name_menu": "menu2",
                        "icon": "build"
                    },
                    {
                        "id_module": 1,
                        "name_module": "users",
                        "name_menu": "menu3",
                        "icon": "build"
                    }
                ]
                           }
           HTTP/1.1 200 Ok
           {
              []
           }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
           HTTP/1.1 404 Not Found
           {
               {
                 "detail": "No encontrado."
               }
           }
    """
    module = 'users'
    map_action = {'list_modules_user': 'pro_consul'}
    queryset = None
    serializer_class = ModuleMinSerializer
    permission_classes = (IsAuthenticatedActive,)

    def list_modules_user(self, request, id):
        Users.get_object(id)
        queryset = Modules.objects.filter(
            Q(id_module__in=ProfileModules.objects.filter(profile_id__in=Profiles.objects.filter(
              id_profile__in=ProfileAdmin.objects.filter(admin_id=Admin.objects.filter(
              id_admin=id).values('id_admin')).values('profile_id')).values('id_profile')).values('module_id')
              )).values('id_module','name_module','name_menu','icon')
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class UserPassword(viewsets.ModelViewSet):
    """
       @api {put} /users/:token/assign_password Asignar Password mediante token enviado al correo.
       @apiParamExample {Json} Request-Put:
            {
                "password": "admin"
                "repeat_password": "admin"
            }
       @apiName PutUserAssignPassword
       @apiGroup UserPassword
       @apiVersion 1.0.0
       @apiParam {Number} token - token enviado al correo del  nuevo usuario
       @apiParam {String} password - contrasena a asignar
       @apiParam {String} repeat_password - confirmar contrasena
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted
           {
               {
                    "message": "Contrasena ha sido asignada exitosamente"
                }
           }
       @apiError (400) {Json} errors Indicando que token enviado ha expirado.
       @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                {
                   {
                        "message": "Se ha enviado un nuevo link para asignar su contrasena",
                        "errors": "Link ha caducado"
                    }
                }
            }
            HTTP/1.1 400 Bad Request
            {
                {
                    "non_field_errors": [
                        "Las contrasenas no coinciden"
                    ]
                }
            }
    """

    serializer_class = UsersPassword
    queryset = None

    def assign_password(self, request, token):
        data = Token_Itsdangerous.confirm_token(token)
        if data['status'] == True:
            instance = Users.get_object(data['token'])
            serializer = self.get_serializer(data=request.data, fields=('password','repeat_password'))
            serializer.is_valid(raise_exception=True)
            self.perform_update(instance, request)
            return Response({"message": "Contrasena ha sido asignada exitosamente"},status=status.HTTP_202_ACCEPTED)
        else:
            user = Admin.objects.filter(id_admin=data['token']).values('id_admin', 'username', 'email')
            data = Token_Itsdangerous.generated_token(user[0]['id_admin'])
            token = URL_VERIFY + data
            password = Gen_Password.password()
            Send_Email.email(user[0]['email'], password, token)
            return Response({"message": "Se ha enviado un nuevo link para asignar su contrasena", "errors": "Link ha caducado"}, status=status.HTTP_400_BAD_REQUEST)

    def perform_update(self,instance,request):
        instance.set_password(request.data['password'])
        instance.save()
