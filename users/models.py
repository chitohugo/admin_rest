from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Admin(AbstractUser):
    id_admin = models.AutoField(primary_key=True)
    modified_date = models.DateTimeField(null=True, blank=True)
    email = models.CharField(max_length=150, unique=True)
    is_active = models.IntegerField(default=1, null=True, blank=True)
    admin_id_create = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='admin_admin_created')
    admin_id_update = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='admin_admin_update')

    class Meta:
        db_table = 'admin'

    def __unicode__(self):
        return unicode(self.id_admin)

    def __str__(self):
        return 'Admin: {} {}'.format(self.id_admin, self.username)
