from django.utils import timezone
from rest_framework import serializers
from users.models import Admin
from groups.models import Groups, GroupAdmin
from profiles.models import Profiles, ProfileAdmin
from modules.models import Modules
from django.contrib.auth.password_validation import validate_password

class DynamicSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        super(DynamicSerializer, self).__init__(*args, **kwargs)
        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

class UserSerializer(DynamicSerializer):
    new_password = serializers.CharField(required=True)
    class Meta:
        model = Admin
        fields = ('id_admin', 'first_name', 'last_name','username', 'email', 'password', 'modified_date', 'date_joined',
                  'is_active', 'new_password','admin_id_create', 'admin_id_update')

    def validate_username(self, username):
        if len(username) > 16 or len(username) < 6:
            raise serializers.ValidationError('El usuario debe contener entre 6 y 16 caracteres')
        return username

    def create(self, validated_data):
        return Admin.objects.create_user(**validated_data)

class UsersPassword(DynamicSerializer):
    repeat_password = serializers.CharField(required=True)
    class Meta:
        model = Admin
        fields = ('password','repeat_password')

    def validate(self, data):
        if data['password'] != data['repeat_password']:
            raise serializers.ValidationError("Las contrasenas no coinciden")
        return data

class UsersMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        fields = ('id_admin', 'username', 'is_active')


class GroupAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupAdmin
        fields = ('group', 'admin')


# Serializers AdminProfiles

class ProfilesMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profiles
        fields = ['id_profile', 'name_profile', 'status_id']


class ProfileAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileAdmin
        fields = ('profile', 'admin')


class ModuleMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modules
        fields = ('id_module','name_module','name_menu','icon')
