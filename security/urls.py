from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url
from security.views import CustomObtainJSONWebToken, RefreshJWT, RenewJWT

custom_obtain_jwt_token = CustomObtainJSONWebToken.as_view()

refresh = RefreshJWT.as_view({
    'post': 'create',
})

renew = RenewJWT.as_view({
    'post': 'create',
})

urlpatterns = [
    url(r'^login$', custom_obtain_jwt_token, name='login'),
    url(r'^login/refresh$', refresh, name='login-refresh'),
    url(r'^login/renew$', renew, name='login-renew'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
