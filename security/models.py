# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

from users.models import Admin


class AdminSessions(models.Model):
    id_admin_session = models.AutoField(primary_key=True)
    admin = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='adminsession')
    session_orig = models.CharField(max_length=256, unique=True)
    start_date = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table = 'admin_session'
        unique_together = ('admin', 'session_orig')

    def __str__(self):
        return self.id_admin_session

