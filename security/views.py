from datetime import datetime
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework_jwt.views import JSONWebTokenAPIView
from rest_framework_jwt.serializers import RefreshJSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings
from security.serializers import CustomJWTSerializer, RenewJSONWebTokenSerializer
from security import permissions

jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class CustomObtainJSONWebToken(JSONWebTokenAPIView):
    """
        @api {post} /login Autenticar usuario.
        @apiParamExample {Json} Request-Create:
            {
                "username": "admin",
                "password": "admin"
            }
        @apiName UserLogin
        @apiGroup Login
        @apiVersion 1.0.0
        @apiPermission NoPermission
        @apiParam {String} username - Nombre del usuario o email del usuario. Es requerido.
        @apiParam {String} password - Password del usuario. Es es requerido.
        @apiSuccess (200) {String} response Ok.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 200 Ok
            {
                "user": "admin",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGJyaWdodHF1ZXN0LnRlY2giLCJvcmlnX2lhdCI6MTUwMjcyMDgwNiwidXNlcl9pZCI6MSwiZXhwIjoxNTAyNzM4ODA2LCJ1c2VybmFtZSI6ImFkbWluIn0.ukWDRep9k91Q7TCdOew57cKzP8dNhsP79GXdb2Eqqmk",
                "user_id": 1
            }
        @apiError (400) {Json} errors Indicando error en los datos ingresados o que campos son requeridos.
        @apiError (404) {Json} errors Indicando que el usuario no existe (o esta inactivo) en la BD.
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "non_field_errors": [
                    "No puede iniciar sesiÃ³n con las credenciales proporcionadas."
                ]
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "username": [
                    "Este campo es requerido."
                ],
                "password": [
                    "Este campo es requerido."
                ]
            }
        @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
                {
                  "detail": "No encontrado."
                }
            }

    """

    serializer_class = CustomJWTSerializer


class RefreshJWT(viewsets.ModelViewSet):
    """
        @api {post} /login/refresh Refrescar el token actual.
        @apiParamExample {Json} Request-Create:
            {
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGJyaWdodHF1ZXN0LnRlY2giLCJvcmlnX2lhdCI6MTUwMjczMTg4NSwidXNlcm5hbWUiOiJhZG1pbiIsImV4cCI6MTUwMjc0OTg4NSwidXNlcl9pZCI6MX0.pH4cZeIAQgX5DizXErnyaGUsbvALDBdDQuvbOh4XuLs"
            }
        @apiHeader {String} Token User unique access-key.
        @apiName UserLoginRefresh
        @apiGroup Login
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive
        @apiParam {String} token - Token actual. Es requerido.
        @apiSuccess (200) {String} response Ok.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 200 Ok
            {
                "user": "admin",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmlnX2lhdCI6MTUwMjczMTg4NSwidXNlcm5hbWUiOiJhZG1pbiIsInVzZXJfaWQiOjEsImV4cCI6MTUwMjc1MDg3NCwiZW1haWwiOiJhZG1pbkBicmlnaHRxdWVzdC50ZWNoIn0.dtxOxsTY1pbyhf8F3ksA9yqs0pJSEh2-DN9x668iwXM",
                "user_id": 1
            }
        @apiError (400) {Json} errors Indicando error en los datos ingresados o que campos son requeridos.
        @apiError (401) {Json} errors Indicando que el token ha espirado.
        @apiError (403) {Json} errors Indicando que el usuario no tiene permisos.
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "non_field_errors": [
                    "Error decoding signature."
                ]
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "token": [
                    "Este campo es requerido."
                ]
            }
        @apiErrorExample {Json} Error-Response 401:
            HTTP/1.1 401 Unauthorized
            {
                "detail": "Signature has expired."
            }
        @apiErrorExample {Json} Error-Response 403:
            HTTP/1.1 403 Forbidden
            {
                "detail": "Usted no tiene permiso para realizar esta acciÃ³n."
            }
    """

    serializer_class = RefreshJSONWebTokenSerializer
    permission_classes = (permissions.IsAuthenticatedActive,)

    def create(self, request, *args, **kwargs):
        '''Permite generar el recurso, en este caso el nvo token'''
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RenewJWT(viewsets.ModelViewSet):
    """
        @api {post} /login/renew Renovar la ventana de tiempo del token actual.
        @apiParamExample {Json} Request-Create:
            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiZW1haWwiOiJhZG1pbkBicmlnaHRxdWVzdC50ZWNoIiwidXNlcl9pZCI6MSwib3JpZ19pYXQiOjE1MDI3MzU0MzYsImV4cCI6MTUwMjczNTkyNX0.IWchGOoP-p9n1MRW7YjicFkHKHiyTTl5x2MK9CD8Qug"
            }
        @apiHeader {String} Token User unique access-key.
        @apiName UserLoginRenew
        @apiGroup Login
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive
        @apiParam {String} token - Ãšltimo token de la ventana actual. Es requerido.
        @apiSuccess (200) {String} response Ok.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 200 Ok
            {
                "user_id": 1,
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiZW1haWwiOiJhZG1pbkBicmlnaHRxdWVzdC50ZWNoIiwidXNlcl9pZCI6MSwib3JpZ19pYXQiOjE1MDI3MzU4MDAsImV4cCI6MTUwMjczNTk4MH0.t-npv2OinQdbEy7VoMbPrD97kcYJFYvoYSkz50ojXIw",
                "user": "admin"
            }
        @apiError (400) {Json} errors Indicando error en los datos ingresados o que campos son requeridos.
        @apiError (403) {Json} errors Indicando que el usuario no tiene permisos.
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "non_field_errors": [
                    "Error decoding signature."
                ]
            }
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "token": [
                    "Este campo es requerido."
                ]
            }
        @apiErrorExample {Json} Error-Response 403:
            HTTP/1.1 403 Forbidden
            {
                "detail": "Usted no tiene permiso para realizar esta acciÃ³n."
            }
    """

    serializer_class = RenewJSONWebTokenSerializer
    permission_classes = (permissions.IsAuthenticatedActive,)

    def create(self, request, *args, **kwargs):
        '''Permite generar el recurso, en este caso el nvo token'''
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)