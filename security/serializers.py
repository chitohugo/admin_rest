from calendar import timegm
from datetime import datetime, timedelta

from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings

from rest_framework_jwt.serializers import JSONWebTokenSerializer, jwt_payload_handler, jwt_encode_handler, \
    VerificationBaseSerializer

from AdminBackend import common
from security.authentication import TokenAuthBackend
from security.models import AdminSessions


class CustomJWTSerializer(JSONWebTokenSerializer):
    """
        Serializer class used to validate a username (o email) and password.

        'username' is identified by the custom UserModel.USERNAME_FIELD.

        Returns a JSON Web Token that can be used to authenticate later calls.
    """
    @property
    def username_field(self):
        return get_username_field()

    @staticmethod
    def __is_email(value):
        try:
            validate_email(value)
            return True
        except ValidationError:
            pass
        return False

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password')
        }

        if all(credentials.values()):
            if self.__is_email(attrs.get(self.username_field)):
                instance = common.Users.get_object_email(attrs.get(self.username_field))
            else:
                instance = common.Users.get_object_username(attrs.get(self.username_field))

            if instance:
                if not instance.is_active == 1:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                user = authenticate(username=instance.username, password=attrs.get('password'))
                if user:
                    payload = jwt_payload_handler(user)

                    """Borramos la sesion anterior y creamos una nueva"""
                    try:
                        AdminSessions.objects.filter(admin=instance.id_admin).delete()
                        newsession = AdminSessions(admin=user, session_orig=payload['orig_iat'])
                        newsession.save()
                        instance.last_login = timezone.now()
                        instance.save()
                    except Exception as e:
                        msg = _('No se pudo crear la session.')
                        raise serializers.ValidationError(msg)

                    return {
                        'token': jwt_encode_handler(payload),
                        'user': user
                    }
            msg = _('Unable to log in with provided credentials.')
            raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class RenewJSONWebTokenSerializer(VerificationBaseSerializer):
    """
    Renew an access token.
    """

    def validate(self, attrs):
        token = attrs['token']
        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)
        # Get and check 'orig_iat'
        orig_iat = payload.get('orig_iat')

        if orig_iat:

            refresh_limit = api_settings.JWT_REFRESH_EXPIRATION_DELTA

            if isinstance(refresh_limit, timedelta):
                refresh_limit = (refresh_limit.days * 24 * 3600 +
                                 refresh_limit.seconds)

            expiration_timestamp = orig_iat + int(refresh_limit)
            now_timestamp = timegm(datetime.utcnow().utctimetuple())

            # Verify if this is the last token refreshed

            if orig_iat <= now_timestamp and now_timestamp >= expiration_timestamp:
                instance = common.Users.get_object_username(user.username)
                if instance:
                    # user = authenticate(username=instance.username)
                    user = TokenAuthBackend.authenticate(username=instance.username)
                    if user:
                        payload = jwt_payload_handler(user)
                        return {
                            'token': jwt_encode_handler(payload),
                            'user': user
                        }
        msg = _('Unable to log in with provided credentials.')
        raise serializers.ValidationError(msg)


