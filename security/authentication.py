import jwt

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import exceptions
from rest_framework_jwt.settings import api_settings
from rest_framework.authentication import (
    BaseAuthentication, get_authorization_header
)
from rest_framework_jwt.utils import jwt_get_username_from_payload_handler

from users.models import Admin
from security.models import AdminSessions

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class TokenAuthBackend(ModelBackend):
    """Log in django with JWT"""

    def authenticate(username=None):
        try:
            return Admin.objects.get(username=username)
        except Admin.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Admin.objects.get(pk=user_id)
        except Admin.DoesNotExist:
            return None


class CustomJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    """
    Token based authentication using the JSON Web Token standard.
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)
        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = _('Firma ha expirado.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _('Error decodificando firma.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        """Verificamos la session"""
        try:
            adminsession = AdminSessions.objects.get(admin=user.id_admin, session_orig=payload['orig_iat'])
        except Exception as e:
            msg = _('El usuario inicio una nueva sesión. Esta sesión ha expirado.')
            raise exceptions.AuthenticationFailed(msg)

        return (user, jwt_value)

