from django.db.models import Q
from rest_framework import permissions, status
from rest_framework.compat import is_authenticated
from rest_framework import viewsets
from modules.models import Modules
from profiles.models import ProfileModules, Profiles, ProfileAdmin
from groups.models import Groups, GroupAdmin, GroupProfile
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


class IsAuthenticatedActive(permissions.BasePermission):
    """
    Allows access only to authenticated and active user.
    """

    def has_permission(self, request, view):
        return request.user and is_authenticated(request.user) and (request.user.is_active == 1)


class HasAccessModuleFunc(permissions.BasePermission):
    """
    Evaluate if user has access to module function 
    """
    def has_permission(self, request, view):
        user = request.user
        try:
            modulefunc = view.map_action[view.action]
            profilemodules = ProfileModules.objects.filter(
                Q(module__in=Modules.objects.filter(name_module=view.module).values('id_module')),
                Q(profile__in=Profiles.objects.filter(status_id=1, id_profile__in=ProfileAdmin.objects.filter(
                    admin_id=user.id_admin).values('profile_id')).values('id_profile')
                  )
                |
                Q(profile__in=Profiles.objects.filter(status_id=1, id_profile__in=GroupProfile.objects.filter(
                    group__in=GroupAdmin.objects.filter(admin_id=user.id_admin, group__in=Groups.objects.filter(
                        status_id=1).values('id_group')).values('group_id')).values('profile_id')).values('id_profile')
                  )
            ).values(modulefunc)
            permission = False
            for profilemodule in profilemodules:
                permission = permission or profilemodule[modulefunc]
            return permission
        except Exception as e:
            return False
