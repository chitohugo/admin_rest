from django.conf.urls import url
from modules import views


module_list = views.ModuleList.as_view({
    'get': 'list',
    'post': 'create'
})

module_solo = views.ModuleSolo.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

module_detail = views.ModuleSolo.as_view({
    'get': 'detail'
})


urlpatterns = [
    url(r'^modules$', module_list, name='modules-list'),
    url(r'^modules/(?P<id>[0-9]+)$', module_solo, name='module-solo'),
    url(r'^modules/(?P<id>[0-9]+)/detail$', module_detail, name='module-detail'),
]
