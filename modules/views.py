from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from modules.models import Modules
from modules.serializers import ModuleSerializer
from AdminBackend import common
from security.permissions import IsAuthenticatedActive, HasAccessModuleFunc

# Create your views here.


class ModuleSolo(viewsets.ModelViewSet):
    """
       @api {get} /modules/:id Obtener un modulo por su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName RetrieveModuleId
       @apiGroup Module
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc 
       @apiParam {Number} id - id del modulo.
       @apiSuccess (200) {String} response OK
       @apiSuccessExample {Json} Success-Response:
            HTTP/1.1 200 Ok
            {               
                {
                    "id_module": 1,
                    "name_module": "users",
                    "name_menu": "menu3",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father_name": null
                }
            }
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found            
            {
              "detail": "No encontrado."
            }            
    """
    """
       @api {get} /modules/:id/detail Obtener mas detalles de un modulo por su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName DetailModuleId
       @apiGroup Module
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc 
       @apiParam {Number} id - id del modulo.
       @apiSuccess (200) {String} response OK
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok           
           {
                {
                    "id_module": 1,
                    "name_module": "users",
                    "name_menu": "menu3",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father": null,
                    "father_name": null
                }
                
           }           
       @apiError (404) {Json} errors Indicando que Id no existe en la bd.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found
            {
              "detail": "No encontrado."
            }
    """
    """
       @api {put} /modules/:id Actualizar modulo por su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName UpdateModuleId
       @apiGroup Module
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc       
       @apiParamExample {Json} Request-Update:
            {
                { 
                    "name_module": "users",
                    "name_menu": "menu3",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father_name": null
                }
            }
       @apiParam {Number} id - id del modulo en la url del request.
       @apiParam {String} name_module - Nombre del modulo, si se cambia debe ser Unico, sino se debe incluir igualmente, ya que es requerido.
       @apiParam {String} name_menu - Nombre del menu, si se cambia debe ser Unico, sino se debe incluir igualmente, ya que es requerido.
       @apiParam {String} icon - Nombre del icono.
       @apiParam {Boolean} mod_consult - Indica si posee menu de consultar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_add - Indica si posee menu de agregar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_edit - Indica si posee menu de editar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_export - Indica si posee menu de exportar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_erase - Indica si posee menu de borrar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_import - Indica si posee menu de importar datos, su valor por default es false. No es requerido.
       @apiParam {Boolean} mod_download - Indica si posee menu de descargar datos, su valor por default es false. No es requerido.
       @apiParam {Number} father_name - Indica el id del modulo padre, si lo tiene. Sino su valor por default es null. No es requerido.
       @apiSuccess (202) {String} response Accepted
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 202 Accepted           
           {
                {
                    "id_module": 1,
                    "name_module": "users",
                    "name_menu": "menu3",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father": null
                }
            }           
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found            
            {
              "detail": "No encontrado."
            }            
       @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
       @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request            
            {
                "name_module": [
                    "modules con esta name module ya existe."
                ]
            }       
       @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request            
            {
                "name_module": [
                    "Este campo es requerido."
                ]
            }            
    """
    """
       @api {delete} /modules/:id Elimina el modulo dado su id.
       @apiHeader {String} Token Users unique access-key.
       @apiName DeleteModuleId
       @apiGroup Module
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
       @apiParam {Number} id - id del modulo.
       @apiSuccess (204) {String} response No Content
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 204 No Content
       @apiError (404) {Json} errors Indicando que Id no existe en la BD.
       @apiErrorExample {Json} Error-Response 404:
            HTTP/1.1 404 Not Found            
            {
              "detail": "No encontrado."
            }            
    """

    module = 'modules'
    map_action = {'retrieve': 'pro_consul', 'detail': 'pro_consul', 'update': 'pro_edit', 'destroy': 'pro_erase'}
    queryset = None
    serializer_class = ModuleSerializer
    permission_classes = (IsAuthenticatedActive,)

    def retrieve(self, request, id):
        """Permite obtener un modulo dado su id"""
        instance = common.Module.get_object(id)
        queryset = Modules.objects.get(id_module=instance.id_module)
        serializer = self.get_serializer(queryset, fields=('id_module', 'name_module','name_menu','icon', 'mod_consult', 'mod_add',
                                                           'mod_edit', 'mod_export', 'mod_erase', 'mod_import',
                                                           'mod_download', 'identify', 'father_name'), many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def detail(self, request, id):
        """Permite obtener detalles del modulo dado su id"""
        instance = common.Module.get_object(id)
        queryset = Modules.objects.get(id_module=instance.id_module)
        serializer = self.get_serializer(queryset, fields=('id_module', 'name_module','name_menu','icon', 'mod_consult', 'mod_add',
                                                           'mod_edit', 'mod_export', 'mod_erase', 'mod_import',
                                                           'mod_download', 'identify', 'father', 'father_name'),
                                         many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, id):
        """Permite actualizar un modulo dado su id"""
        instance = common.Module.get_object(id)
        serializer = self.get_serializer(instance, fields=('id_module', 'name_module', 'name_menu','icon', 'mod_consult', 'mod_add',
                                                           'mod_edit', 'mod_export', 'mod_erase', 'mod_import',
                                                           'mod_download', 'father'), data=request.data)

        '''Se debe validar que el id del father no sea igual a su propio id o de uno de sus descendientes
        para no crear un ciclo'''

        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_update(self, serializer):
        serializer.save()

    def destroy(self, request, id):
        """Permite borrar de manera logica el modulo dado su id"""
        instance = common.Module.get_object(id)
        Modules.objects.filter(id_module=instance.id_module).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


'''Clase de la api para listar los modulos y crear un modulo'''


class ModuleList(viewsets.ModelViewSet):
    """
       @api {get} /modules Listar los modulo registrados
       @apiHeader {String} Token User unique access-key.
       @apiName ListModules
       @apiGroup Module
       @apiVersion 1.0.0
       @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
       @apiSuccess (200) {String} response Listado de modulo registrados.
       @apiSuccessExample {Json} Success-Response:
           HTTP/1.1 200 Ok           
           [
                {
                    "id_module": 2,
                    "name_module": "groups",
                    "name_menu": "menu2",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father_name": null
                },
                {
                    "id_module": 3,
                    "name_module": "modules",
                    "name_menu": "menu1",
                    "icon": "build",
                    "mod_consult": true,
                    "mod_add": true,
                    "mod_edit": true,
                    "mod_export": false,
                    "mod_erase": true,
                    "mod_import": false,
                    "mod_download": true,
                    "father_name": null
                },
           ]
    """
    """ 
        @api {post} /modules Registrar Modulo
        @apiParamExample {Json} Request-Create:
            {
                "name_module": "users",
                "name_menu": "menu3",
                "icon": "build",
                "mod_consult": true,
                "mod_add": true,
                "mod_edit": true,
                "mod_export": false,
                "mod_erase": true,
                "mod_import": false,
                "mod_download": true,
                "father_name": null
            }
        @apiHeader {String} Token User unique access-key.
        @apiName CreateModule
        @apiGroup Module
        @apiVersion 1.0.0
        @apiPermission IsAuthenticatedActive, HasAccessModuleFunc
        @apiParam {String} name_module - Nombre del modulo, debe ser unico y es requerido
        @apiParam {String} name_menu - Nombre del menu, debe ser unico y es requerido
        @apiParam {String} icon - Nombre del icono
        @apiParam {Boolean} mod_consult - Indica si posee menu de consultar datos, su valor por default es false
        @apiParam {Boolean} mod_add - Indica si posee menu de agregar datos, su valor por default es false
        @apiParam {Boolean} mod_edit - Indica si posee menu de editar datos, su valor por default es false
        @apiParam {Boolean} mod_export - Indica si posee menu de exportar datos, su valor por default es false
        @apiParam {Boolean} mod_erase - Indica si posee menu de borrar datos, su valor por default es false
        @apiParam {Boolean} mod_import - Indica si posee menu de importar datos, su valor por default es false
        @apiParam {Boolean} mod_download - Indica si posee menu de descargar datos, su valor por default es false
        @apiParam {Number} father_name - Indica el id del mÃ³dulo padre, si lo tiene. Sino su valor por default es null  

        @apiSuccess (201) {String} response Created.
        @apiSuccessExample {string} Success-Response:
            HTTP/1.1 201 Created
                        {
                                "id": 8
                        }
        @apiError (400) {Json} errors Indicando que campos ya existen y/o son requeridos.
        @apiErrorExample {Json} Error-Response 400:
            HTTP/1.1 400 Bad Request
            {
                "name_module": [
                    "modules con esta name module ya existe."
                ]                
            }        
        @apiErrorExample {Json} Error-Response 400:        
            HTTP/1.1 400 Bad Request            
            {
                "name_module": [
                    "Este campo es requerido."
                ]
            }                      
    """

    module = 'modules'
    map_action = {'list': 'pro_consul', 'create': 'pro_add'}
    queryset = Modules.objects.all().order_by('name_module')
    serializer_class = ModuleSerializer
    permission_classes = (IsAuthenticatedActive,)

    def list(self, request, *args, **kwargs):
        """Permite obtener la lista de todos los modulos"""
        queryset = self.filter_queryset(self.get_queryset())
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #      serializer = self.get_serializer(page, fields=('id_admin', 'first_name', 'last_name', 'username', 'email', 'date_joined'), many=True)
        #      return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, fields=('id_module', 'name_module','name_menu','icon', 'mod_consult', 'mod_add',
                                                           'mod_edit', 'mod_export', 'mod_erase', 'mod_import',
                                                           'mod_download', 'father_name'), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Permite crear un modulo"""
        serializer = self.get_serializer(data=request.data, fields=('id_module', 'name_module','name_menu','icon', 'mod_consult',
                                                                    'mod_add', 'mod_edit', 'mod_export', 'mod_erase',
                                                                    'mod_import', 'mod_download', 'father'))
        serializer.is_valid(raise_exception=True)
        module = self.perform_create(serializer)
        return Response({'id': module.id_module}, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()
