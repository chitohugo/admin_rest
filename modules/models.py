# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.


class Modules(models.Model):
    id_module = models.AutoField(primary_key=True)
    name_module = models.CharField(max_length=50, unique=True)
    name_menu = models.CharField(max_length=50, unique=True)
    icon = models.CharField(max_length=50)
    mod_consult = models.BooleanField(default=False)
    mod_add = models.BooleanField(default=False)
    mod_edit = models.BooleanField(default=False)
    mod_export = models.BooleanField(default=False)
    mod_erase = models.BooleanField(default=False)
    mod_import = models.BooleanField(default=False)
    mod_download = models.BooleanField(default=False)
    father = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='modulefather')

    class Meta:
        db_table = 'modules'

    def __str__(self):
        return self.id_module
